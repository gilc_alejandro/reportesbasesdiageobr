
DECLARE @startDate Datetime = '2017-12-17', @endDate Datetime = '2017-12-23'

DECLARE @formTypeCode Varchar(10) = 'EX' --EX

;WITH t 
AS
(
	select lc.locationName AS PDV
	, lc.locationReference AS CNPJ
	, lc.locationId
	, Products.[Form Product List Name] AS [Codigo Produto]
	, Products.[Form Product List Category] AS [Categoria Producto]
	, Products.[Form Product List Standard Name] AS [Descripcion Producto]
	, DATENAME(MONTH, @endDate)+'/'+CAST(YEAR(@endDate) AS VARCHAR(4)) AS M�s
		, CAST(DAY(@startDate) AS VARCHAR(2))+'/'+CAST(MONTH(@startDate) AS VARCHAR(2))+' a '+CAST(DAY(@endDate) AS VARCHAR(2))+'/'+CAST(MONTH(@endDate) AS VARCHAR(2)) AS Semana
	, @formTypeCode AS [Form Type Code]
	from [DW].LocationsBronzeDiciembre as l
	inner join [DW].Location as lc on l.CNPJ = lc.locationReference
	inner join
	(
			select pl.*
			from [DW].ProductosBronzeNew as p
			inner join 
			(
					select av.attributeValue as [Form Product List Name], 
					(
						select av2.attributeValue
						from [System].tbl_Attributes_Values as av2
						where av2.categoryItemId = av.categoryItemId and av2.attributeId = 94
					) as [Form Product List Category], 
					(
						select av2.attributeValue
						from [System].tbl_Attributes_Values as av2
						where av2.categoryItemId = av.categoryItemId and av2.attributeId = 99
					) as [Form Product List Standard Name]
					from [System].tbl_Category_Items as ci
					inner join [System].tbl_Attributes_Values as av on ci.categoryItemId = av.categoryItemId
					where ci.categoryId = 51 and av.attributeId = 91

			) as pl on p.Codigo = pl.[Form Product List Name]

	) as Products on 1 = 1
)

SELECT l.*
	/*,(
		SELECT MAX(pr.auditedBy)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.[Form Product List Name] = l.[Codigo Produto]
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					AND pr.planResultDetailIsDeleted = 0
					AND pr.[Form Template Form Type Code] = @formTypeCode
	) AS auditedBy*/
	, COALESCE(
				CAST
				(
					(
						SELECT MAX(pr.measureAnswer)
						FROM [DW].vw_PRDFIO AS pr
						WHERE pr.locationId = l.locationId 
						AND pr.[Form Product List Name] = l.[Codigo Produto]
						AND pr.dateAudited BETWEEN @startDate AND @endDate
						AND pr.[Form Sales Drivers Name] = 'DISTRIBUI��O'
						AND pr.isCompleted = 1
						AND pr.planResultDetailIsDeleted = 0
						AND pr.formItemOptionText = 'SIM'
						AND pr.[Form Template Form Type Code] = @formTypeCode
						AND pr.auditedBy NOT LIKE '%test%'
					) 
					AS VARCHAR(50)
				)
				,
				CAST
				( 
					0 * (
						SELECT MAX(pr.measureAnswer)
						FROM [DW].vw_PRDFIO AS pr
						WHERE pr.locationId = l.locationId 
						AND pr.[Form Product List Name] = l.[Codigo Produto]
						AND pr.dateAudited BETWEEN @startDate AND @endDate
						AND pr.[Form Sales Drivers Name] = 'DISTRIBUI��O'
						AND pr.isCompleted = 1
						AND pr.planResultDetailIsDeleted = 0
						AND pr.formItemOptionText <> 'SIM'
						AND pr.[Form Template Form Type Code] = @formTypeCode
						AND pr.auditedBy NOT LIKE '%test%'
					)
				AS VARCHAR(50)
				)
				, 'NA' 
	) AS [Presen�a]
	, COALESCE(
			CAST(
					(
						SELECT AVG(pr.measureAnswer)
						FROM [DW].vw_PRDFIO AS pr
						WHERE pr.locationId = l.locationId 
						AND pr.[Form Product List Name] = l.[Codigo Produto]
						AND pr.dateAudited BETWEEN @startDate AND @endDate
						AND pr.[Form Sales Drivers Name] = 'PRE�O'
						AND pr.isCompleted = 1
						AND pr.measureAnswer > 0
						AND pr.planResultDetailIsDeleted = 0
						AND pr.[Form Template Form Type Code] = @formTypeCode
						AND pr.auditedBy NOT LIKE '%test%'
					)
					AS VARCHAR(50) )
					, 'NA'
	) AS [Pre�o]
	, COALESCE(
			CAST(
					(
						SELECT AVG(pr.measureAnswer)
						FROM [DW].vw_PRDFIO AS pr
						WHERE pr.locationId = l.locationId 
						AND pr.[Form Product List Name] = l.[Codigo Produto]
						AND pr.dateAudited BETWEEN @startDate AND @endDate
						AND pr.[Form Sales Drivers Name] = 'PRE�O'
						AND pr.isCompleted = 1
						AND pr.measureAnswer > 0
						AND pr.[Form Pre�o Name] = 'DOSE'
						AND pr.planResultDetailIsDeleted = 0
						AND pr.[Form Template Form Type Code] = @formTypeCode
						AND pr.auditedBy NOT LIKE '%test%'
					)
					AS VARCHAR(50) )
					, 'NA'
	) AS [Pre�o Dose]
	, COALESCE(
		CAST(
			(
					SELECT AVG(pr.measureAnswer)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.[Form Product List Name] = l.[Codigo Produto]
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.[Form Sales Drivers Name] = 'PRE�O'
					AND pr.isCompleted = 1
					AND pr.measureAnswer > 0
					AND pr.[Form Pre�o Name] = 'GARRAFA'
					AND pr.planResultDetailIsDeleted = 0
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
			)
			AS VARCHAR(50))
		, 'NA'
	) AS [Pre�o Garrafa]
	, (
		COALESCE(
		CAST(
			(
				SELECT AVG(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId 
				AND pr.[Form Product List Name] = l.[Codigo Produto]
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.[Form Sales Drivers Name] = 'PRE�O'
				AND pr.isCompleted = 1
				AND pr.measureAnswer > 0
				AND pr.[Form Pre�o Name] = 'DRINK'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
		)
			AS VARCHAR(50)
		), 'NA')
	) AS [Pre�o Drink]
	, COALESCE(
				CAST(
				(
					SELECT MAX(pr.measureAnswer)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					--AND pr.formItemStatement = 'O BAR POSSUI O MENU DIAGEO (FOLDER) EXECUTADO NA MESA ?'
					AND pr.[Form Visibilidade Name] = 'Menu Diageo (Folder)'
					AND pr.formItemOptionText = 'SIM'
					AND pr.planResultDetailIsDeleted = 0
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				) AS VARCHAR(50))
				,
				CAST(
				0 * (
					SELECT MAX(pr.measureAnswer)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					--AND pr.formItemStatement = 'O BAR POSSUI O MENU DIAGEO (FOLDER) EXECUTADO NA MESA ?'
					AND pr.[Form Visibilidade Name] = 'Menu Diageo (Folder)'
					AND pr.formItemOptionText <> 'SIM'
					AND pr.planResultDetailIsDeleted = 0
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				) AS VARCHAR(50))
				, 'NA'
		) AS [Menu Diageo (Folder)]
	, COALESCE(
				CAST(
				(
					SELECT MAX(pr.measureAnswer)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					--AND pr.[Form Sales Drivers Name] = 'VISIBILIDADE'
					--AND pr.formItemStatement = 'O BAR CONHECE A MECANICA DO PROGRAMA E SUA PONTUA��O ATUAL ?'
					AND pr.[Form Visibilidade Name] = 'Pontua��o Atual'
					AND pr.planResultDetailIsDeleted = 0
					AND pr.formItemOptionText = 'SIM'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				) AS VARCHAR(50))
				, CAST(
				0 * (
					SELECT MAX(pr.measureAnswer)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					--AND pr.[Form Sales Drivers Name] = 'VISIBILIDADE'
					--AND pr.formItemStatement = 'O BAR CONHECE A MECANICA DO PROGRAMA E SUA PONTUA��O ATUAL ?'
					AND pr.[Form Visibilidade Name] = 'Pontua��o Atual'
					AND pr.planResultDetailIsDeleted = 0
					AND pr.formItemOptionText <> 'SIM'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				) AS VARCHAR(50))
				, 'NA'	
		) AS [Conhecimento do Programa Top Bar]
	, COALESCE(
				CAST(
				(
					SELECT MAX(pr.measureAnswer)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					--AND pr.formItemStatement = 'O BAR ESTA ADERIDO AO PROGRAMA DE RELACIONAMENTO TOP BAR ?'
					AND pr.[Form Visibilidade Name] = 'Relacionamiento'
					AND pr.planResultDetailIsDeleted = 0
					AND pr.formItemOptionText = 'SIM'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				) AS VARCHAR(50))
				, CAST(
				0 * (
					SELECT MAX(pr.measureAnswer)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					--AND pr.formItemStatement = 'O BAR ESTA ADERIDO AO PROGRAMA DE RELACIONAMENTO TOP BAR ?'
					AND pr.[Form Visibilidade Name] = 'Relacionamiento'
					AND pr.planResultDetailIsDeleted = 0
					AND pr.formItemOptionText <> 'SIM'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				) AS VARCHAR(50))
				, 'NA'
		) AS [Programa Top Bar]
	, COALESCE(
				CAST
				(
					(
						SELECT MAX(pr.measureAnswer)
						FROM [DW].vw_PRDFIO AS pr
						WHERE pr.locationId = l.locationId 
						AND pr.dateAudited BETWEEN @startDate AND @endDate
						--AND pr.formItemStatement = 'O BAR TEM CLUBE DO WHISKY IMPLEMENTADO?'
						AND pr.[Form Promo��o Name] = 'Clube do Whisky'
						AND pr.isCompleted = 1
						AND pr.planResultDetailIsDeleted = 0
						AND pr.formItemOptionText = 'SIM'
						AND pr.[Form Template Form Type Code] = @formTypeCode
						AND pr.auditedBy NOT LIKE '%test%'
					) 
					AS VARCHAR(50)
				)
				,
				CAST
				( 
					0 * (
						SELECT MAX(pr.measureAnswer)
						FROM [DW].vw_PRDFIO AS pr
						WHERE pr.locationId = l.locationId 
						AND pr.dateAudited BETWEEN @startDate AND @endDate
						--AND pr.formItemStatement = 'O BAR TEM CLUBE DO WHISKY IMPLEMENTADO?'
						AND pr.[Form Promo��o Name] = 'Clube do Whisky'
						AND pr.isCompleted = 1
						AND pr.planResultDetailIsDeleted = 0
						AND pr.formItemOptionText <> 'SIM'
						AND pr.[Form Template Form Type Code] = @formTypeCode
						AND pr.auditedBy NOT LIKE '%test%'
					)
				AS VARCHAR(50)
				)
				, 'NA' 
	) AS [Clube do Whisky]
	, COALESCE(
				CAST
				(
					(
						SELECT MAX(pr.measureAnswer)
						FROM [DW].vw_PRDFIO AS pr
						WHERE pr.locationId = l.locationId 
						AND pr.dateAudited BETWEEN @startDate AND @endDate
						--AND pr.formItemStatement IN ('SELECIONE AS MARCAS QUE EST�O PRESENTES NO CLUBE DO WISKY', 'QUAIS AS MARCAS PRESENTES NO CLUBE DO WHISKY?')
						AND pr.[Form Promo��o Name] = 'Clube do Whisky - Marcas'
						AND pr.isCompleted = 1
						AND pr.planResultDetailIsDeleted = 0
						AND pr.formItemOptionText = 'JW RED LABEL'
						AND pr.[Form Template Form Type Code] = @formTypeCode
						AND pr.auditedBy NOT LIKE '%test%'
					) 
					AS VARCHAR(50)
				)
				,
				CAST
				( 
					0 * (
						SELECT MAX(pr.measureAnswer)
						FROM [DW].vw_PRDFIO AS pr
						WHERE pr.locationId = l.locationId 
						AND pr.dateAudited BETWEEN @startDate AND @endDate
						--AND pr.formItemStatement IN ('SELECIONE AS MARCAS QUE EST�O PRESENTES NO CLUBE DO WISKY', 'QUAIS AS MARCAS PRESENTES NO CLUBE DO WHISKY?')
						AND pr.[Form Promo��o Name] = 'Clube do Whisky - Marcas'
						AND pr.isCompleted = 1
						AND pr.planResultDetailIsDeleted = 0
						AND pr.formItemOptionText <> 'JW RED LABEL'
						AND pr.[Form Template Form Type Code] = @formTypeCode
						AND pr.auditedBy NOT LIKE '%test%'
					)
				AS VARCHAR(50)
				)
				, 'NA' 
	) AS [Clube do Whisky - JW RED LABEL]
	, COALESCE(
				CAST
				(
					(
						SELECT MAX(pr.measureAnswer)
						FROM [DW].vw_PRDFIO AS pr
						WHERE pr.locationId = l.locationId 
						AND pr.dateAudited BETWEEN @startDate AND @endDate
						--AND pr.formItemStatement IN ('SELECIONE AS MARCAS QUE EST�O PRESENTES NO CLUBE DO WHISKY', 'QUAIS AS MARCAS PRESENTES NO CLUBE DO WHISKY?')
						AND pr.[Form Promo��o Name] = 'Clube do Whisky - Marcas'
						AND pr.isCompleted = 1
						AND pr.planResultDetailIsDeleted = 0
						AND pr.formItemOptionText = 'JW BLACK LABEL'
						AND pr.[Form Template Form Type Code] = @formTypeCode
						AND pr.auditedBy NOT LIKE '%test%'
					) 
					AS VARCHAR(50)
				)
				,
				CAST
				( 
					0 * (
						SELECT MAX(pr.measureAnswer)
						FROM [DW].vw_PRDFIO AS pr
						WHERE pr.locationId = l.locationId 
						AND pr.dateAudited BETWEEN @startDate AND @endDate
						--AND pr.formItemStatement IN ('SELECIONE AS MARCAS QUE EST�O PRESENTES NO CLUBE DO WHISKY', 'QUAIS AS MARCAS PRESENTES NO CLUBE DO WHISKY?')
						AND pr.[Form Promo��o Name] = 'Clube do Whisky - Marcas'
						AND pr.isCompleted = 1
						AND pr.planResultDetailIsDeleted = 0
						AND pr.formItemOptionText <> 'JW BLACK LABEL'
						AND pr.[Form Template Form Type Code] = @formTypeCode
						AND pr.auditedBy NOT LIKE '%test%'
					)
				AS VARCHAR(50)
				)
				, 'NA' 
	) AS [Clube do Whisky - JW BLACK LABEL]
	, COALESCE(
				CAST
				(
					(
						SELECT MAX(pr.measureAnswer)
						FROM [DW].vw_PRDFIO AS pr
						WHERE pr.locationId = l.locationId 
						AND pr.dateAudited BETWEEN @startDate AND @endDate
						--AND pr.formItemStatement IN ('SELECIONE AS MARCAS QUE EST�O PRESENTES NO CLUBE DO WHISKY', 'QUAIS AS MARCAS PRESENTES NO CLUBE DO WHISKY?')
						AND pr.[Form Promo��o Name] = 'Clube do Whisky - Marcas'
						AND pr.isCompleted = 1
						AND pr.planResultDetailIsDeleted = 0
						AND pr.formItemOptionText = 'OLD PARR'
						AND pr.[Form Template Form Type Code] = @formTypeCode
						AND pr.auditedBy NOT LIKE '%test%'
					) 
					AS VARCHAR(50)
				)
				,
				CAST
				( 
					0 * (
						SELECT MAX(pr.measureAnswer)
						FROM [DW].vw_PRDFIO AS pr
						WHERE pr.locationId = l.locationId 
						AND pr.dateAudited BETWEEN @startDate AND @endDate
						--AND pr.formItemStatement IN ('SELECIONE AS MARCAS QUE EST�O PRESENTES NO CLUBE DO WHISKY', 'QUAIS AS MARCAS PRESENTES NO CLUBE DO WHISKY?')
						AND pr.[Form Promo��o Name] = 'Clube do Whisky - Marcas'
						AND pr.isCompleted = 1
						AND pr.planResultDetailIsDeleted = 0
						AND pr.formItemOptionText <> 'OLD PARR'
						AND pr.[Form Template Form Type Code] = @formTypeCode
						AND pr.auditedBy NOT LIKE '%test%'
					)
				AS VARCHAR(50)
				)
				, 'NA' 
	) AS [Clube do Whisky - OLD PARR]
	, COALESCE(
				CAST
				(
					(
						SELECT MAX(pr.measureAnswer)
						FROM [DW].vw_PRDFIO AS pr
						WHERE pr.locationId = l.locationId 
						AND pr.dateAudited BETWEEN @startDate AND @endDate
						--AND pr.formItemStatement IN ('SELECIONE AS MARCAS QUE EST�O PRESENTES NO CLUBE DO WHISKY', 'QUAIS AS MARCAS PRESENTES NO CLUBE DO WHISKY?')
						AND pr.[Form Promo��o Name] = 'Clube do Whisky - Marcas'
						AND pr.isCompleted = 1
						AND pr.planResultDetailIsDeleted = 0
						AND pr.formItemOptionText = 'BLACK & WHITE'
						AND pr.[Form Template Form Type Code] = @formTypeCode
						AND pr.auditedBy NOT LIKE '%test%'
					) 
					AS VARCHAR(50)
				)
				,
				CAST
				( 
					0 * (
						SELECT MAX(pr.measureAnswer)
						FROM [DW].vw_PRDFIO AS pr
						WHERE pr.locationId = l.locationId 
						AND pr.dateAudited BETWEEN @startDate AND @endDate
						--AND pr.formItemStatement IN ('SELECIONE AS MARCAS QUE EST�O PRESENTES NO CLUBE DO WHISKY', 'QUAIS AS MARCAS PRESENTES NO CLUBE DO WHISKY?')
						AND pr.[Form Promo��o Name] = 'Clube do Whisky - Marcas'
						AND pr.isCompleted = 1
						AND pr.planResultDetailIsDeleted = 0
						AND pr.formItemOptionText <> 'BLACK & WHITE'
						AND pr.[Form Template Form Type Code] = @formTypeCode
						AND pr.auditedBy NOT LIKE '%test%'
					)
					AS VARCHAR(50)
				)
				, 'NA' 
	) AS [Clube do Whisky - BLACK & WHITE]
	, COALESCE(
				CAST
				(
					(
						SELECT MAX(pr.measureAnswer)
						FROM [DW].vw_PRDFIO AS pr
						WHERE pr.locationId = l.locationId 
						AND pr.dateAudited BETWEEN @startDate AND @endDate
						--AND pr.formItemStatement IN ('SELECIONE AS MARCAS QUE EST�O PRESENTES NO CLUBE DO WHISKY', 'QUAIS AS MARCAS PRESENTES NO CLUBE DO WHISKY?')
						AND pr.[Form Promo��o Name] = 'Clube do Whisky - Marcas'
						AND pr.isCompleted = 1
						AND pr.planResultDetailIsDeleted = 0
						AND pr.formItemOptionText = 'WHITE HORSE'
						AND pr.[Form Template Form Type Code] = @formTypeCode
						AND pr.auditedBy NOT LIKE '%test%'
					) 
					AS VARCHAR(50)
				)
				,
				CAST
				( 
					0 * (
						SELECT MAX(pr.measureAnswer)
						FROM [DW].vw_PRDFIO AS pr
						WHERE pr.locationId = l.locationId 
						AND pr.dateAudited BETWEEN @startDate AND @endDate
						--AND pr.formItemStatement IN ('SELECIONE AS MARCAS QUE EST�O PRESENTES NO CLUBE DO WHISKY', 'QUAIS AS MARCAS PRESENTES NO CLUBE DO WHISKY?')
						AND pr.[Form Promo��o Name] = 'Clube do Whisky - Marcas'
						AND pr.isCompleted = 1
						AND pr.planResultDetailIsDeleted = 0
						AND pr.formItemOptionText <> 'WHITE HORSE'
						AND pr.[Form Template Form Type Code] = @formTypeCode
						AND pr.auditedBy NOT LIKE '%test%'
					)
					AS VARCHAR(50)
				)
				, 'NA' 
	) AS [Clube do Whisky - WHITE HORSE]
	, COALESCE(
				CAST
				(
					(
						SELECT MAX(pr.measureAnswer)
						FROM [DW].vw_PRDFIO AS pr
						WHERE pr.locationId = l.locationId 
						AND pr.dateAudited BETWEEN @startDate AND @endDate
						--AND pr.formItemStatement IN ('SELECIONE AS MARCAS QUE EST�O PRESENTES NO CLUBE DO WHISKY', 'QUAIS AS MARCAS PRESENTES NO CLUBE DO WHISKY?')
						AND pr.[Form Promo��o Name] = 'Clube do Whisky - Marcas'
						AND pr.isCompleted = 1
						AND pr.planResultDetailIsDeleted = 0
						AND pr.formItemOptionText = 'OUTROS'
						AND pr.[Form Template Form Type Code] = @formTypeCode
						AND pr.auditedBy NOT LIKE '%test%'
					) 
					AS VARCHAR(50)
				)
				,
				CAST
				( 
					0 * (
						SELECT MAX(pr.measureAnswer)
						FROM [DW].vw_PRDFIO AS pr
						WHERE pr.locationId = l.locationId 
						AND pr.dateAudited BETWEEN @startDate AND @endDate
						--AND pr.formItemStatement IN ('SELECIONE AS MARCAS QUE EST�O PRESENTES NO CLUBE DO WHISKY', 'QUAIS AS MARCAS PRESENTES NO CLUBE DO WHISKY?')
						AND pr.[Form Promo��o Name] = 'Clube do Whisky - Marcas'
						AND pr.isCompleted = 1
						AND pr.planResultDetailIsDeleted = 0
						AND pr.formItemOptionText <> 'OUTROS'
						AND pr.[Form Template Form Type Code] = @formTypeCode
						AND pr.auditedBy NOT LIKE '%test%'
					)
					AS VARCHAR(50)
				)
				, 'NA' 
	) AS [Clube do Whisky - OUTROS]
	, COALESCE(
				CAST
				(
					(
						SELECT MAX(pr.measureAnswer)
						FROM [DW].vw_PRDFIO AS pr
						WHERE pr.locationId = l.locationId 
						AND pr.dateAudited BETWEEN @startDate AND @endDate
						--AND pr.formItemStatement = 'O BAR POSSUI O TABLE TENT E A��O "CONCURSO MELHOR CAIPIROSKA DE SMIRNOFF + JW WEEKEND" IMPLEMENTADA ?'
						AND pr.[Form Visibilidade Name] = 'Table Tent "Melhor Caipiroska"'
						AND pr.isCompleted = 1
						AND pr.planResultDetailIsDeleted = 0
						AND pr.formItemOptionText = 'SIM'
						AND pr.[Form Template Form Type Code] = @formTypeCode
						AND pr.auditedBy NOT LIKE '%test%'
					) 
					AS VARCHAR(50)
				)
				,
				CAST
				( 
					0 * (
						SELECT MAX(pr.measureAnswer)
						FROM [DW].vw_PRDFIO AS pr
						WHERE pr.locationId = l.locationId 
						AND pr.dateAudited BETWEEN @startDate AND @endDate
						--AND pr.formItemStatement = 'O BAR POSSUI O TABLE TENT E A��O "CONCURSO MELHOR CAIPIROSKA DE SMIRNOFF + JW WEEKEND" IMPLEMENTADA ?'
						AND pr.[Form Visibilidade Name] = 'Table Tent "Melhor Caipiroska"'
						AND pr.isCompleted = 1
						AND pr.planResultDetailIsDeleted = 0
						AND pr.formItemOptionText <> 'SIM'
						AND pr.[Form Template Form Type Code] = @formTypeCode
						AND pr.auditedBy NOT LIKE '%test%'
					)
					AS VARCHAR(50)
				)
				, 'NA' 
	) AS [Table Tent "Melhor Caipiroska"]
	, COALESCE(
				CAST
				(
					(
						SELECT MAX(pr.measureAnswer)
						FROM [DW].vw_PRDFIO AS pr
						WHERE pr.locationId = l.locationId 
						AND pr.dateAudited BETWEEN @startDate AND @endDate
						--AND pr.formItemStatement = 'O BAR POSSUI O TABLE TENT E A��O "JW DESCUBRA SEU SABOR + DUPLA DE CAIPIROSKAS SMIRNOFF" IMPLEMENTADA ?'
						AND pr.[Form Visibilidade Name] = 'Table Tent "Descubra Seu Sabor"'
						AND pr.isCompleted = 1
						AND pr.planResultDetailIsDeleted = 0
						AND pr.formItemOptionText = 'SIM'
						AND pr.[Form Template Form Type Code] = @formTypeCode
						AND pr.auditedBy NOT LIKE '%test%'
					) 
					AS VARCHAR(50)
				)
				,
				CAST
				( 
					0 * (
						SELECT MAX(pr.measureAnswer)
						FROM [DW].vw_PRDFIO AS pr
						WHERE pr.locationId = l.locationId 
						AND pr.dateAudited BETWEEN @startDate AND @endDate
						--AND pr.formItemStatement = 'O BAR POSSUI O TABLE TENT E A��O "JW DESCUBRA SEU SABOR + DUPLA DE CAIPIROSKAS SMIRNOFF" IMPLEMENTADA ?'
						AND pr.[Form Visibilidade Name] = 'Table Tent "Descubra Seu Sabor"'
						AND pr.isCompleted = 1
						AND pr.planResultDetailIsDeleted = 0
						AND pr.formItemOptionText <> 'SIM'
						AND pr.[Form Template Form Type Code] = @formTypeCode
						AND pr.auditedBy NOT LIKE '%test%'
					)
					AS VARCHAR(50)
				)
				, 'NA' 
	) AS [Table Tent "Descubra Seu Sabor"]
	, COALESCE(
				CAST
				(
					(
						SELECT MAX(pr.measureAnswer)
						FROM [DW].vw_PRDFIO AS pr
						WHERE pr.locationId = l.locationId 
						AND pr.dateAudited BETWEEN @startDate AND @endDate
						--AND pr.formItemStatement = 'O BAR POSSUI O TABLE TENT E A��O "DUPLA DE CAIPIROSKAS SMIRNOFF + JW WEEKEND" IMPLEMENTADA ?'
						AND pr.[Form Visibilidade Name] = 'Table Tent "Dupla de Caipiroskas"'
						AND pr.isCompleted = 1
						AND pr.planResultDetailIsDeleted = 0
						AND pr.formItemOptionText = 'SIM'
						AND pr.[Form Template Form Type Code] = @formTypeCode
						AND pr.auditedBy NOT LIKE '%test%'
					) 
					AS VARCHAR(50)
				)
				,
				CAST
				( 
					0 * (
						SELECT MAX(pr.measureAnswer)
						FROM [DW].vw_PRDFIO AS pr
						WHERE pr.locationId = l.locationId 
						AND pr.dateAudited BETWEEN @startDate AND @endDate
						--AND pr.formItemStatement = 'O BAR POSSUI O TABLE TENT E A��O "DUPLA DE CAIPIROSKAS SMIRNOFF + JW WEEKEND" IMPLEMENTADA ?'
						AND pr.[Form Visibilidade Name] = 'Table Tent "Dupla de Caipiroskas"'
						AND pr.isCompleted = 1
						AND pr.planResultDetailIsDeleted = 0
						AND pr.formItemOptionText <> 'SIM'
						AND pr.[Form Template Form Type Code] = @formTypeCode
						AND pr.auditedBy NOT LIKE '%test%'
					)
					AS VARCHAR(50)
				)
				, 'NA' 
	) AS [Table Tent "Dupla de Caipiroskas"]
	, COALESCE(
				CAST
				(
					(
						SELECT MAX(pr.measureAnswer)
						FROM [DW].vw_PRDFIO AS pr
						WHERE pr.locationId = l.locationId 
						AND pr.dateAudited BETWEEN @startDate AND @endDate
						AND pr.formItemStatement = 'O BAR ESTA FUNCIONANDO ?'
						--AND pr.[Form Performance Name] = 'Bar Funcionando'
						AND pr.isCompleted = 1
						AND pr.planResultDetailIsDeleted = 0
						AND pr.formItemOptionText = 'SIM'
						AND pr.[Form Template Form Type Code] = @formTypeCode
						AND pr.auditedBy NOT LIKE '%test%'
					) 
					AS VARCHAR(50)
				)
				,
				CAST
				( 
					0 * (
						SELECT MAX(pr.measureAnswer)
						FROM [DW].vw_PRDFIO AS pr
						WHERE pr.locationId = l.locationId 
						AND pr.dateAudited BETWEEN @startDate AND @endDate
						AND pr.formItemStatement = 'O BAR ESTA FUNCIONANDO ?'
						--AND pr.[Form Performance Name] = 'Bar Funcionando'
						AND pr.isCompleted = 1
						AND pr.planResultDetailIsDeleted = 0
						AND pr.formItemOptionText <> 'SIM'
						AND pr.[Form Template Form Type Code] = @formTypeCode
						AND pr.auditedBy NOT LIKE '%test%'
					)
					AS VARCHAR(50)
				)
				, 'NA' 
	) AS [BAR FUNCIONANDO]
	, COALESCE(
				CAST(
				(
					SELECT MAX(CASE WHEN pr.measureAnswer > 0 THEN 1 ELSE 0 END)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					AND pr.formItemStatement = 'QUAL O MOTIVO DO BAR N�O ESTAR FUNCIONANDO ?'
					--AND pr.[Form Visibilidade Name] = 'Bar n�o funcionando'
					AND pr.planResultDetailIsDeleted = 0
					AND pr.formItemOptionText = 'FORA DO HOR�RIO DE ATENDIMENTO'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				) AS VARCHAR(50))
				,  CAST(
				0 * (
					SELECT MAX(CASE WHEN pr.measureAnswer > 0 THEN 1 ELSE 0 END)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					AND pr.formItemStatement = 'QUAL O MOTIVO DO BAR N�O ESTAR FUNCIONANDO ?'
					--AND pr.[Form Visibilidade Name] = 'Bar n�o funcionando'
					AND pr.planResultDetailIsDeleted = 0
					AND pr.formItemOptionText <> 'FORA DO HOR�RIO DE ATENDIMENTO'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				) AS VARCHAR(50))
				, 'NA'
	) AS [Bar n�o funcionando - FORA DO HOR�RIO DE ATENDIMENTO]
	, COALESCE(
				CAST(
				(
					SELECT MAX(CASE WHEN pr.measureAnswer > 0 THEN 1 ELSE 0 END)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					AND pr.formItemStatement = 'QUAL O MOTIVO DO BAR N�O ESTAR FUNCIONANDO ?'
					--AND pr.[Form Visibilidade Name] = 'Bar n�o funcionando'
					AND pr.planResultDetailIsDeleted = 0
					AND pr.formItemOptionText = 'BAR FECHOU'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				) AS VARCHAR(50))
				,  CAST(
				0 * (
					SELECT MAX(CASE WHEN pr.measureAnswer > 0 THEN 1 ELSE 0 END)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					AND pr.formItemStatement = 'QUAL O MOTIVO DO BAR N�O ESTAR FUNCIONANDO ?'
					--AND pr.[Form Visibilidade Name] = 'Bar n�o funcionando'
					AND pr.planResultDetailIsDeleted = 0
					AND pr.formItemOptionText <> 'BAR FECHOU'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				) AS VARCHAR(50))
				, 'NA'
	) AS [Bar n�o funcionando - BAR FECHOU]
	, COALESCE(
				CAST(
				(
					SELECT MAX(CASE WHEN pr.measureAnswer > 0 THEN 1 ELSE 0 END)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					AND pr.formItemStatement = 'QUAL O MOTIVO DO BAR N�O ESTAR FUNCIONANDO ?'
					--AND pr.[Form Visibilidade Name] = 'Bar n�o funcionando'
					AND pr.planResultDetailIsDeleted = 0
					AND pr.formItemOptionText = 'REFORMA'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				) AS VARCHAR(50))
				,  CAST(
				0 * (
					SELECT MAX(CASE WHEN pr.measureAnswer > 0 THEN 1 ELSE 0 END)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					AND pr.formItemStatement = 'QUAL O MOTIVO DO BAR N�O ESTAR FUNCIONANDO ?'
					--AND pr.[Form Visibilidade Name] = 'Bar n�o funcionando'
					AND pr.planResultDetailIsDeleted = 0
					AND pr.formItemOptionText <> 'REFORMA'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				) AS VARCHAR(50))
				, 'NA'
	) AS [Bar n�o funcionando - REFORMA]
FROM t AS l
