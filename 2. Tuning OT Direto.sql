
CREATE STATISTICS [_dta_stat_941883814_1_2_3_65] ON [DW].[Form]([formGroupId], [formItemId], [formItemOptionId], [Form Qualidade Name])

CREATE STATISTICS [_dta_stat_941883814_1_2_3_66] ON [DW].[Form]([formGroupId], [formItemId], [formItemOptionId], [Form Visibilidade Name])

CREATE STATISTICS [_dta_stat_941883814_1_2_3_38] ON [DW].[Form]([formGroupId], [formItemId], [formItemOptionId], [Form Sales Drivers Name])

CREATE STATISTICS [_dta_stat_941883814_1_2_3_64] ON [DW].[Form]([formGroupId], [formItemId], [formItemOptionId], [Form Pre�o Name])

SET ANSI_PADDING ON

CREATE NONCLUSTERED INDEX [_dta_index_Form_7_941883814__K66_1_2_3_33] ON [DW].[Form]
(
	[Form Visibilidade Name] ASC
)
INCLUDE ( 	[formGroupId],
	[formItemId],
	[formItemOptionId],
	[Form Template Form Type Code]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [FG_Data1]

SET ANSI_PADDING ON

CREATE NONCLUSTERED INDEX [_dta_index_Form_7_941883814__K1_K2_K3_8] ON [DW].[Form]
(
	[formGroupId] ASC,
	[formItemId] ASC,
	[formItemOptionId] ASC
)
INCLUDE ( 	[formItemStatement]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [FG_Data1]

SET ANSI_PADDING ON

CREATE NONCLUSTERED INDEX [_dta_index_Form_7_941883814__K1_K2_K3_K33_66] ON [DW].[Form]
(
	[formGroupId] ASC,
	[formItemId] ASC,
	[formItemOptionId] ASC,
	[Form Template Form Type Code] ASC
)
INCLUDE ( 	[Form Visibilidade Name]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [FG_Data1]

SET ANSI_PADDING ON

CREATE NONCLUSTERED INDEX [_dta_index_Form_7_941883814__K33_1_2_3_38_40] ON [DW].[Form]
(
	[Form Template Form Type Code] ASC
)
INCLUDE ( 	[formGroupId],
	[formItemId],
	[formItemOptionId],
	[Form Sales Drivers Name],
	[Form Product List Name]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [FG_Data1]

CREATE STATISTICS [_dta_stat_941883814_38_1_2] ON [DW].[Form]([Form Sales Drivers Name], [formGroupId], [formItemId])

SET ANSI_PADDING ON

CREATE NONCLUSTERED INDEX [_dta_index_Form_7_941883814__K1_K2_K3_K40_33_38] ON [DW].[Form]
(
	[formGroupId] ASC,
	[formItemId] ASC,
	[formItemOptionId] ASC,
	[Form Product List Name] ASC
)
INCLUDE ( 	[Form Template Form Type Code],
	[Form Sales Drivers Name]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [FG_Data1]

SET ANSI_PADDING ON

CREATE NONCLUSTERED INDEX [_dta_index_Form_7_941883814__K33_1_2_3_38_40_64] ON [DW].[Form]
(
	[Form Template Form Type Code] ASC
)
INCLUDE ( 	[formGroupId],
	[formItemId],
	[formItemOptionId],
	[Form Sales Drivers Name],
	[Form Product List Name],
	[Form Pre�o Name]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [FG_Data1]

SET ANSI_PADDING ON

CREATE NONCLUSTERED INDEX [_dta_index_Form_7_941883814__K40_1_2_3_33_38_64] ON [DW].[Form]
(
	[Form Product List Name] ASC
)
INCLUDE ( 	[formGroupId],
	[formItemId],
	[formItemOptionId],
	[Form Template Form Type Code],
	[Form Sales Drivers Name],
	[Form Pre�o Name]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [FG_Data1]

SET ANSI_PADDING ON

CREATE NONCLUSTERED INDEX [_dta_index_Form_7_941883814__K1_K2_K3_K33_8_38_40_64_65_66] ON [DW].[Form]
(
	[formGroupId] ASC,
	[formItemId] ASC,
	[formItemOptionId] ASC,
	[Form Template Form Type Code] ASC
)
INCLUDE ( 	[formItemStatement],
	[Form Sales Drivers Name],
	[Form Product List Name],
	[Form Pre�o Name],
	[Form Qualidade Name],
	[Form Visibilidade Name]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [FG_Data1]

SET ANSI_PADDING ON

CREATE NONCLUSTERED INDEX [_dta_index_Form_7_941883814__K33_1_2_3_8_38_40_64_65_66] ON [DW].[Form]
(
	[Form Template Form Type Code] ASC
)
INCLUDE ( 	[formGroupId],
	[formItemId],
	[formItemOptionId],
	[formItemStatement],
	[Form Sales Drivers Name],
	[Form Product List Name],
	[Form Pre�o Name],
	[Form Qualidade Name],
	[Form Visibilidade Name]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [FG_Data1]

SET ANSI_PADDING ON

CREATE NONCLUSTERED INDEX [_dta_index_Form_7_941883814__K64_1_2_3_8_33_38_40_65_66] ON [DW].[Form]
(
	[Form Pre�o Name] ASC
)
INCLUDE ( 	[formGroupId],
	[formItemId],
	[formItemOptionId],
	[formItemStatement],
	[Form Template Form Type Code],
	[Form Sales Drivers Name],
	[Form Product List Name],
	[Form Qualidade Name],
	[Form Visibilidade Name]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [FG_Data1]

SET ANSI_PADDING ON

CREATE NONCLUSTERED INDEX [_dta_index_Form_7_941883814__K65_1_2_3_8_33_38_40_64_66] ON [DW].[Form]
(
	[Form Qualidade Name] ASC
)
INCLUDE ( 	[formGroupId],
	[formItemId],
	[formItemOptionId],
	[formItemStatement],
	[Form Template Form Type Code],
	[Form Sales Drivers Name],
	[Form Product List Name],
	[Form Pre�o Name],
	[Form Visibilidade Name]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [FG_Data1]

CREATE NONCLUSTERED INDEX [_dta_index_Location_7_2145364099__K1] ON [DW].[Location]
(
	[locationId] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [FG_Data1]

SET ANSI_PADDING ON

CREATE NONCLUSTERED INDEX [_dta_index_Location_7_2145364099__K6_K1_2] ON [DW].[Location]
(
	[locationReference] ASC,
	[locationId] ASC
)
INCLUDE ( 	[locationName]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [FG_Data1]

SET ANSI_PADDING ON

CREATE NONCLUSTERED INDEX [_dta_index_Location_7_2145364099__K1_K6_2] ON [DW].[Location]
(
	[locationId] ASC,
	[locationReference] ASC
)
INCLUDE ( 	[locationName]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [FG_Data1]

SET ANSI_PADDING ON

CREATE NONCLUSTERED INDEX [_dta_index_LocationsOTDiretoNew_7_298405524__K1] ON [DW].[LocationsOTDiretoNew]
(
	[CNPJ] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [FG_Data1]

CREATE NONCLUSTERED INDEX [_dta_index_Plan_7_13880508__K2_K1] ON [DW].[Plan]
(
	[locationId] ASC,
	[devicePlanId] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [FG_Data1]

CREATE NONCLUSTERED INDEX [_dta_index_Plan_7_13880508__K2] ON [DW].[Plan]
(
	[locationId] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [FG_Data1]

CREATE NONCLUSTERED INDEX [_dta_index_Plan_7_13880508__K1_K2] ON [DW].[Plan]
(
	[devicePlanId] ASC,
	[locationId] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [FG_Data1]

CREATE STATISTICS [_dta_stat_1133884498_23_8_22_17_2] ON [DW].[PlanResult]([planResultDetailIsDeleted], [isCompleted], [measureAnswer], [dateAudited], [devicePlanId])

CREATE STATISTICS [_dta_stat_1133884498_23_8_22_17_3_4_5_6] ON [DW].[PlanResult]([planResultDetailIsDeleted], [isCompleted], [measureAnswer], [dateAudited], [locationId], [formGroupId], [formItemId], [formItemOptionId])

CREATE STATISTICS [_dta_stat_1133884498_22_4_5_6_17_23_20] ON [DW].[PlanResult]([measureAnswer], [formGroupId], [formItemId], [formItemOptionId], [dateAudited], [planResultDetailIsDeleted], [formAnswerValue])

CREATE STATISTICS [_dta_stat_1133884498_20_23_8_22_17_2] ON [DW].[PlanResult]([formAnswerValue], [planResultDetailIsDeleted], [isCompleted], [measureAnswer], [dateAudited], [devicePlanId])

CREATE STATISTICS [_dta_stat_1133884498_20_23_8_22_17_4_5] ON [DW].[PlanResult]([formAnswerValue], [planResultDetailIsDeleted], [isCompleted], [measureAnswer], [dateAudited], [formGroupId], [formItemId])

CREATE STATISTICS [_dta_stat_1133884498_4_5_6_17_3_2_23_8] ON [DW].[PlanResult]([formGroupId], [formItemId], [formItemOptionId], [dateAudited], [locationId], [devicePlanId], [planResultDetailIsDeleted], [isCompleted])

CREATE STATISTICS [_dta_stat_1133884498_4_5_6_23_8_22] ON [DW].[PlanResult]([formGroupId], [formItemId], [formItemOptionId], [planResultDetailIsDeleted], [isCompleted], [measureAnswer])

CREATE STATISTICS [_dta_stat_1133884498_4_5_6_17_3_2_23_20] ON [DW].[PlanResult]([formGroupId], [formItemId], [formItemOptionId], [dateAudited], [locationId], [devicePlanId], [planResultDetailIsDeleted], [formAnswerValue])

CREATE STATISTICS [_dta_stat_1133884498_23_8_22_17_4_5_6_20] ON [DW].[PlanResult]([planResultDetailIsDeleted], [isCompleted], [measureAnswer], [dateAudited], [formGroupId], [formItemId], [formItemOptionId], [formAnswerValue])

CREATE STATISTICS [_dta_stat_1133884498_3_23_8_22] ON [DW].[PlanResult]([locationId], [planResultDetailIsDeleted], [isCompleted], [measureAnswer])

CREATE STATISTICS [_dta_stat_1133884498_17_4_5_6_23_20] ON [DW].[PlanResult]([dateAudited], [formGroupId], [formItemId], [formItemOptionId], [planResultDetailIsDeleted], [formAnswerValue])

SET ANSI_PADDING ON

CREATE NONCLUSTERED INDEX [_dta_index_PlanResult_7_1133884498__K3_K20_K23_K8_K22_K17_K4_K5_K6_K2] ON [DW].[PlanResult]
(
	[locationId] ASC,
	[formAnswerValue] ASC,
	[planResultDetailIsDeleted] ASC,
	[isCompleted] ASC,
	[measureAnswer] ASC,
	[dateAudited] ASC,
	[formGroupId] ASC,
	[formItemId] ASC,
	[formItemOptionId] ASC,
	[devicePlanId] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [FG_Data1]

SET ANSI_PADDING ON

CREATE NONCLUSTERED INDEX [_dta_index_PlanResult_7_1133884498__K4_K5_K6_K20_K23_K8_K22_K17_K3_K2] ON [DW].[PlanResult]
(
	[formGroupId] ASC,
	[formItemId] ASC,
	[formItemOptionId] ASC,
	[formAnswerValue] ASC,
	[planResultDetailIsDeleted] ASC,
	[isCompleted] ASC,
	[measureAnswer] ASC,
	[dateAudited] ASC,
	[locationId] ASC,
	[devicePlanId] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [FG_Data1]

SET ANSI_PADDING ON

CREATE NONCLUSTERED INDEX [_dta_index_PlanResult_7_1133884498__K20_K23_K8_K22_K17_K3_K4_K5_K6_K2] ON [DW].[PlanResult]
(
	[formAnswerValue] ASC,
	[planResultDetailIsDeleted] ASC,
	[isCompleted] ASC,
	[measureAnswer] ASC,
	[dateAudited] ASC,
	[locationId] ASC,
	[formGroupId] ASC,
	[formItemId] ASC,
	[formItemOptionId] ASC,
	[devicePlanId] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [FG_Data1]

SET ANSI_PADDING ON

CREATE NONCLUSTERED INDEX [_dta_index_PlanResult_7_1133884498__K2_K3_K20_K23_K8_K22_K17_K4_K5_K6] ON [DW].[PlanResult]
(
	[devicePlanId] ASC,
	[locationId] ASC,
	[formAnswerValue] ASC,
	[planResultDetailIsDeleted] ASC,
	[isCompleted] ASC,
	[measureAnswer] ASC,
	[dateAudited] ASC,
	[formGroupId] ASC,
	[formItemId] ASC,
	[formItemOptionId] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [FG_Data1]

SET ANSI_PADDING ON

CREATE NONCLUSTERED INDEX [_dta_index_PlanResult_7_1133884498__K2_K3_K23_K8_K22_K17_K4_K5_K6_K20] ON [DW].[PlanResult]
(
	[devicePlanId] ASC,
	[locationId] ASC,
	[planResultDetailIsDeleted] ASC,
	[isCompleted] ASC,
	[measureAnswer] ASC,
	[dateAudited] ASC,
	[formGroupId] ASC,
	[formItemId] ASC,
	[formItemOptionId] ASC,
	[formAnswerValue] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [FG_Data1]

CREATE STATISTICS [_dta_stat_1133884498_17_3_2_23_20_8_22_4_5] ON [DW].[PlanResult]([dateAudited], [locationId], [devicePlanId], [planResultDetailIsDeleted], [formAnswerValue], [isCompleted], [measureAnswer], [formGroupId], [formItemId])

CREATE STATISTICS [_dta_stat_1133884498_17_3_2_23_8] ON [DW].[PlanResult]([dateAudited], [locationId], [devicePlanId], [planResultDetailIsDeleted], [isCompleted])

CREATE STATISTICS [_dta_stat_1133884498_17_23_8_22_3_2_4_5_6] ON [DW].[PlanResult]([dateAudited], [planResultDetailIsDeleted], [isCompleted], [measureAnswer], [locationId], [devicePlanId], [formGroupId], [formItemId], [formItemOptionId])

SET ANSI_PADDING ON

CREATE NONCLUSTERED INDEX [_dta_index_PlanResult_7_1133884498__K8_K4_K5_K6_K17_K23_K20_K3_K2_K22] ON [DW].[PlanResult]
(
	[isCompleted] ASC,
	[formGroupId] ASC,
	[formItemId] ASC,
	[formItemOptionId] ASC,
	[dateAudited] ASC,
	[planResultDetailIsDeleted] ASC,
	[formAnswerValue] ASC,
	[locationId] ASC,
	[devicePlanId] ASC,
	[measureAnswer] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [FG_Data1]

CREATE STATISTICS [_dta_stat_1133884498_17_23_20_8] ON [DW].[PlanResult]([dateAudited], [planResultDetailIsDeleted], [formAnswerValue], [isCompleted])

CREATE STATISTICS [_dta_stat_1540837943_4_2] ON [System].[tbl_Attributes_Values]([attributeValue], [categoryItemId])

CREATE STATISTICS [_dta_stat_1540837943_1_4_2] ON [System].[tbl_Attributes_Values]([attributeId], [attributeValue], [categoryItemId])

SET ANSI_PADDING ON

CREATE NONCLUSTERED INDEX [_dta_index_tbl_Attributes_Values_7_1540837943__K1_K2_K4] ON [System].[tbl_Attributes_Values]
(
	[attributeId] ASC,
	[categoryItemId] ASC,
	[attributeValue] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [FG_Data1]

CREATE NONCLUSTERED INDEX [_dta_index_tbl_Category_Items_7_1589580701__K2_K1] ON [System].[tbl_Category_Items]
(
	[categoryId] ASC,
	[categoryItemId] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [FG_Data1]

