
DECLARE @startDate Datetime = '2017-12-17', @endDate Datetime = '2017-12-23'
DECLARE @formTypeCode Varchar(10) = 'EX' --EX

;WITH t 
AS
(
	select lc.locationName AS PDV
	, lc.locationReference AS CNPJ
	, lc.locationId
	, Products.[Form Product List Name] AS [Codigo Produto]
	, Products.[Form Product List Category] AS [Categoria Producto]
	, Products.[Form Product List Standard Name] AS [Descripcion Producto]
	, DATENAME(MONTH, @endDate)+'/'+CAST(YEAR(@endDate) AS VARCHAR(4)) AS M�s
		, CAST(DAY(@startDate) AS VARCHAR(2))+'/'+CAST(MONTH(@startDate) AS VARCHAR(2))+' a '+CAST(DAY(@endDate) AS VARCHAR(2))+'/'+CAST(MONTH(@endDate) AS VARCHAR(2)) AS Semana
	from [DW].LocationsMassMerchantDiciembre as l
	inner join [DW].Location as lc on l.CNPJ = lc.locationReference
	inner join
	(
			select pl.*
			from [DW].ProductosMassMerchantNew as p
			inner join 
			(
					select av.attributeValue as [Form Product List Name], 
					(
						select av2.attributeValue
						from [System].tbl_Attributes_Values as av2
						where av2.categoryItemId = av.categoryItemId and av2.attributeId = 94
					) as [Form Product List Category], 
					(
						select av2.attributeValue
						from [System].tbl_Attributes_Values as av2
						where av2.categoryItemId = av.categoryItemId and av2.attributeId = 99
					) as [Form Product List Standard Name]
					from [System].tbl_Category_Items as ci
					inner join [System].tbl_Attributes_Values as av on ci.categoryItemId = av.categoryItemId
					where ci.categoryId = 51 and av.attributeId = 91

			) as pl on p.Codigo = pl. [Form Product List Name]

	) as Products on 1 = 1
)

SELECT l.*
	/*,(
		SELECT MAX(pr.auditedBy)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.[Form Product List Name] = l.[Codigo Produto]
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					AND pr.planResultDetailIsDeleted = 0
					AND pr.[Form Template Form Type Code] = @formTypeCode
	) AS auditedBy*/
	, ISNULL(
				(
					SELECT MAX(pr.measureAnswer)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.[Form Product List Name] = l.[Codigo Produto]
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.[Form Sales Drivers Name] = 'DISTRIBUI��O'
					AND pr.isCompleted = 1
					AND pr.planResultDetailIsDeleted = 0
					AND pr.formItemOptionText = 'SIM'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				)
				, 
				0 * (
					SELECT MAX(pr.measureAnswer)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.[Form Product List Name] = l.[Codigo Produto]
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.[Form Sales Drivers Name] = 'DISTRIBUI��O'
					AND pr.isCompleted = 1
					AND pr.planResultDetailIsDeleted = 0
					AND pr.formItemOptionText <> 'SIM'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				)
	) AS [Presen�a]
	, (
			SELECT AVG(pr.measureAnswer)
			FROM [DW].vw_PRDFIO AS pr
			WHERE pr.locationId = l.locationId 
			AND pr.[Form Product List Name] = l.[Codigo Produto]
			AND pr.dateAudited BETWEEN @startDate AND @endDate
			AND pr.[Form Sales Drivers Name] = 'PRE�O'
			AND pr.isCompleted = 1
			AND pr.measureAnswer > 0
			AND pr.planResultDetailIsDeleted = 0
			AND pr.[Form Template Form Type Code] = @formTypeCode
			AND pr.auditedBy NOT LIKE '%test%'
	) AS [Pre�o]
	, (
		SELECT AVG(pr.measureAnswer)
		FROM [DW].vw_PRDFIO AS pr
		WHERE pr.locationId = l.locationId 
		AND pr.[Form Product List Name] = l.[Codigo Produto]
		AND pr.dateAudited BETWEEN @startDate AND @endDate
		AND pr.[Form Sales Drivers Name] = 'PRE�O'
		AND pr.isCompleted = 1
		AND pr.measureAnswer > 0
		AND pr.[Form Pre�o Name] = 'DOSE'
		AND pr.planResultDetailIsDeleted = 0
		AND pr.[Form Template Form Type Code] = @formTypeCode
		AND pr.auditedBy NOT LIKE '%test%'
	) AS [Pre�o Dose]
	, ISNULL(
				(
					SELECT MAX(CASE WHEN pr.measureAnswer > 0 THEN 1 ELSE 0 END)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					--AND pr.formItemStatement = 'EXISTE DISPLAY DOSADOR DA DIAGEO IMPLEMENTADO NO BAR ?'
					AND pr.[Form Visibilidade Name] = 'Display Dosador'
					AND pr.planResultDetailIsDeleted = 0
					AND pr.formItemOptionText = 'SIM'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				)
				, 
				0 * (
					SELECT MAX(CASE WHEN pr.measureAnswer > 0 THEN 1 ELSE 0 END)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					--AND pr.formItemStatement = 'EXISTE DISPLAY DOSADOR DA DIAGEO IMPLEMENTADO NO BAR ?'
					AND pr.[Form Visibilidade Name] = 'Display Dosador'
					AND pr.planResultDetailIsDeleted = 0
					AND pr.formItemOptionText <> 'SIM'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				)
	) AS [Display Dosador]
	, ISNULL(
				(
					SELECT MAX(CASE WHEN pr.measureAnswer > 0 THEN 1 ELSE 0 END)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					--AND pr.formItemStatement = 'EXISTE CARTAZ PORTIFOLIO DA DIAGEO IMPLEMENTADO NO BAR ?'
					AND pr.[Form Visibilidade Name] = 'Portifolio Cartaz'
					AND pr.planResultDetailIsDeleted = 0
					AND pr.formItemOptionText = 'SIM'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				)
				, 
				0 * (
					SELECT MAX(CASE WHEN pr.measureAnswer > 0 THEN 1 ELSE 0 END)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					--AND pr.formItemStatement = 'EXISTE CARTAZ PORTIFOLIO DA DIAGEO IMPLEMENTADO NO BAR ?'
					AND pr.[Form Visibilidade Name] = 'Portifolio Cartaz'
					AND pr.planResultDetailIsDeleted = 0
					AND pr.formItemOptionText <> 'SIM'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				)
	) AS [Cartaz Portfolio]
	, ISNULL(
				(
					SELECT MAX(CASE WHEN pr.measureAnswer > 0 THEN 1 ELSE 0 END)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					--AND pr.formItemStatement = 'EXISTE BANDO DA DIAGEO IMPLEMENTADO NO BAR ?'
					AND pr.[Form Visibilidade Name] = 'Bando Diageo'
					AND pr.planResultDetailIsDeleted = 0
					AND pr.formItemOptionText = 'SIM'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				)
				, 
				0 * (
					SELECT MAX(CASE WHEN pr.measureAnswer > 0 THEN 1 ELSE 0 END)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					--AND pr.formItemStatement = 'EXISTE BANDO DA DIAGEO IMPLEMENTADO NO BAR ?'
					AND pr.[Form Visibilidade Name] = 'Bando Diageo'
					AND pr.planResultDetailIsDeleted = 0
					AND pr.formItemOptionText <> 'SIM'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				)
	) AS [Bando]
	, ISNULL(
				(
					SELECT MAX(CASE WHEN pr.measureAnswer > 0 THEN 1 ELSE 0 END)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					--AND pr.formItemStatement = 'O BAR ESTA PINTADO COM ALGUMA MARCA ?'
					AND pr.[Form Visibilidade Name] = 'Pintura'
					AND pr.planResultDetailIsDeleted = 0
					AND pr.formItemOptionText = 'SIM'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				)
				, 
				0 * (
					SELECT MAX(CASE WHEN pr.measureAnswer > 0 THEN 1 ELSE 0 END)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					--AND pr.formItemStatement = 'O BAR ESTA PINTADO COM ALGUMA MARCA ?'
					AND pr.[Form Visibilidade Name] = 'Pintura'
					AND pr.planResultDetailIsDeleted = 0
					AND pr.formItemOptionText <> 'SIM'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				)
	) AS [Pintura]
	, ISNULL(
				(
					SELECT MAX(CASE WHEN pr.measureAnswer > 0 THEN 1 ELSE 0 END)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					--AND pr.formItemStatement = 'COM QUAL (IS) MARCA (S) O BAR EST� PINTADO ?'
					AND pr.[Form Visibilidade Name] = 'Pintura - Marcas'
					AND pr.planResultDetailIsDeleted = 0
					AND pr.formItemOptionText = 'DIAGEO YPIOCA'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				)
				, 
				0 * (
					SELECT MAX(CASE WHEN pr.measureAnswer > 0 THEN 1 ELSE 0 END)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					--AND pr.formItemStatement = 'COM QUAL (IS) MARCA (S) O BAR EST� PINTADO ?'
					AND pr.[Form Visibilidade Name] = 'Pintura - Marcas'
					AND pr.planResultDetailIsDeleted = 0
					AND pr.formItemOptionText <> 'DIAGEO YPIOCA'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				)
	) AS [Pintura - Ypioca]
	, ISNULL(
				(
					SELECT MAX(CASE WHEN pr.measureAnswer > 0 THEN 1 ELSE 0 END)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					--AND pr.formItemStatement = 'COM QUAL (IS) MARCA (S) O BAR EST� PINTADO ?'
					AND pr.[Form Visibilidade Name] = 'Pintura - Marcas'
					AND pr.planResultDetailIsDeleted = 0
					AND pr.formItemOptionText = 'DIAGEO SAPUPARA'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				)
				, 
				0 * (
					SELECT MAX(CASE WHEN pr.measureAnswer > 0 THEN 1 ELSE 0 END)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					--AND pr.formItemStatement = 'COM QUAL (IS) MARCA (S) O BAR EST� PINTADO ?'
					AND pr.[Form Visibilidade Name] = 'Pintura - Marcas'
					AND pr.planResultDetailIsDeleted = 0
					AND pr.formItemOptionText <> 'DIAGEO SAPUPARA'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				)
	) AS [Pintura - Sapupara]
	, ISNULL(
				(
					SELECT MAX(CASE WHEN pr.measureAnswer > 0 THEN 1 ELSE 0 END)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					--AND pr.formItemStatement = 'COM QUAL (IS) MARCA (S) O BAR EST� PINTADO ?'
					AND pr.[Form Visibilidade Name] = 'Pintura - Marcas'
					AND pr.planResultDetailIsDeleted = 0
					AND pr.formItemOptionText = 'CONCORRENTE 51'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				)
				, 
				0 * (
					SELECT MAX(CASE WHEN pr.measureAnswer > 0 THEN 1 ELSE 0 END)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					--AND pr.formItemStatement = 'COM QUAL (IS) MARCA (S) O BAR EST� PINTADO ?'
					AND pr.[Form Visibilidade Name] = 'Pintura - Marcas'
					AND pr.planResultDetailIsDeleted = 0
					AND pr.formItemOptionText <> 'CONCORRENTE 51'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				)
	) AS [Pintura - 51]
	, ISNULL(
				(
					SELECT MAX(CASE WHEN pr.measureAnswer > 0 THEN 1 ELSE 0 END)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					--AND pr.formItemStatement = 'COM QUAL (IS) MARCA (S) O BAR EST� PINTADO ?'
					AND pr.[Form Visibilidade Name] = 'Pintura - Marcas'
					AND pr.planResultDetailIsDeleted = 0
					AND pr.formItemOptionText = 'CONCORRENTE PITU'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				)
				, 
				0 * (
					SELECT MAX(CASE WHEN pr.measureAnswer > 0 THEN 1 ELSE 0 END)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					--AND pr.formItemStatement = 'COM QUAL (IS) MARCA (S) O BAR EST� PINTADO ?'
					AND pr.[Form Visibilidade Name] = 'Pintura - Marcas'
					AND pr.planResultDetailIsDeleted = 0
					AND pr.formItemOptionText <> 'CONCORRENTE PITU'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				)
	) AS [Pintura - Pitu]
	, ISNULL(
				(
					SELECT MAX(CASE WHEN pr.measureAnswer > 0 THEN 1 ELSE 0 END)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					--AND pr.formItemStatement = 'COM QUAL (IS) MARCA (S) O BAR EST� PINTADO ?'
					AND pr.[Form Visibilidade Name] = 'Pintura - Marcas'
					AND pr.planResultDetailIsDeleted = 0
					AND pr.formItemOptionText = 'CERVEJARIA'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				)
				, 
				0 * (
					SELECT MAX(CASE WHEN pr.measureAnswer > 0 THEN 1 ELSE 0 END)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					--AND pr.formItemStatement = 'COM QUAL (IS) MARCA (S) O BAR EST� PINTADO ?'
					AND pr.[Form Visibilidade Name] = 'Pintura - Marcas'
					AND pr.planResultDetailIsDeleted = 0
					AND pr.formItemOptionText <> 'CERVEJARIA'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				)
	) AS [Pintura - Cervejaria]
	, ISNULL(
				(
					SELECT MAX(CASE WHEN pr.measureAnswer > 0 THEN 1 ELSE 0 END)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					--AND pr.formItemStatement = 'COM QUAL (IS) MARCA (S) O BAR EST� PINTADO ?'
					AND pr.[Form Visibilidade Name] = 'Pintura - Marcas'
					AND pr.planResultDetailIsDeleted = 0
					AND pr.formItemOptionText = 'REFRIGERANTE'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				)
				, 
				0 * (
					SELECT MAX(CASE WHEN pr.measureAnswer > 0 THEN 1 ELSE 0 END)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					--AND pr.formItemStatement = 'COM QUAL (IS) MARCA (S) O BAR EST� PINTADO ?'
					AND pr.[Form Visibilidade Name] = 'Pintura - Marcas'
					AND pr.planResultDetailIsDeleted = 0
					AND pr.formItemOptionText <> 'REFRIGERANTE'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				)
	) AS [Pintura - Refrigerante]
	, ISNULL(
				(
					SELECT MAX(CASE WHEN pr.measureAnswer > 0 THEN 1 ELSE 0 END)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					--AND pr.formItemStatement = 'COM QUAL (IS) MARCA (S) O BAR EST� PINTADO ?'
					AND pr.[Form Visibilidade Name] = 'Pintura - Marcas'
					AND pr.planResultDetailIsDeleted = 0
					AND pr.formItemOptionText = 'CARANGUEJO'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				)
				, 
				0 * (
					SELECT MAX(CASE WHEN pr.measureAnswer > 0 THEN 1 ELSE 0 END)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					--AND pr.formItemStatement = 'COM QUAL (IS) MARCA (S) O BAR EST� PINTADO ?'
					AND pr.[Form Visibilidade Name] = 'Pintura - Marcas'
					AND pr.planResultDetailIsDeleted = 0
					AND pr.formItemOptionText <> 'CARANGUEJO'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				)
	) AS [Pintura - Carangueijo]
	, ISNULL(
				(
					SELECT MAX(CASE WHEN pr.measureAnswer > 0 THEN 1 ELSE 0 END)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					--AND pr.formItemStatement = 'COM QUAL (IS) MARCA (S) O BAR EST� PINTADO ?'
					AND pr.[Form Visibilidade Name] = 'Pintura - Marcas'
					AND pr.planResultDetailIsDeleted = 0
					AND pr.formItemOptionText = 'N�O ESTA PINTADO'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				)
				, 
				0 * (
					SELECT MAX(CASE WHEN pr.measureAnswer > 0 THEN 1 ELSE 0 END)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					--AND pr.formItemStatement = 'COM QUAL (IS) MARCA (S) O BAR EST� PINTADO ?'
					AND pr.[Form Visibilidade Name] = 'Pintura - Marcas'
					AND pr.planResultDetailIsDeleted = 0
					AND pr.formItemOptionText <> 'N�O ESTA PINTADO'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				)
	) AS [Pintura - N�o Esta Pintado]
	, ISNULL(
				(
					SELECT MAX(CASE WHEN pr.measureAnswer > 0 THEN 1 ELSE 0 END)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					--AND pr.formItemStatement = 'TEM ALGUMA ATIVIDADE PROMOCIONAL DA DIAGEO ACONTECENDO NO BAR ?'
					AND pr.[Form Visibilidade Name] = 'Actividade Promocional'
					AND pr.planResultDetailIsDeleted = 0
					AND pr.formItemOptionText = 'CAIXA MISTA'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				)
				, 
				0 * (
					SELECT MAX(CASE WHEN pr.measureAnswer > 0 THEN 1 ELSE 0 END)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					--AND pr.formItemStatement = 'TEM ALGUMA ATIVIDADE PROMOCIONAL DA DIAGEO ACONTECENDO NO BAR ?'
					AND pr.[Form Visibilidade Name] = 'Actividade Promocional'
					AND pr.planResultDetailIsDeleted = 0
					AND pr.formItemOptionText <> 'CAIXA MISTA'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				)
	) AS [Atividade Promocional - Caixa Mista]
	, ISNULL(
				(
					SELECT MAX(CASE WHEN pr.measureAnswer > 0 THEN 1 ELSE 0 END)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					--AND pr.formItemStatement = 'TEM ALGUMA ATIVIDADE PROMOCIONAL DA DIAGEO ACONTECENDO NO BAR ?'
					AND pr.[Form Visibilidade Name] = 'Actividade Promocional'
					AND pr.planResultDetailIsDeleted = 0
					AND pr.formItemOptionText = 'DEGUSTA��O'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				)
				, 
				0 * (
					SELECT MAX(CASE WHEN pr.measureAnswer > 0 THEN 1 ELSE 0 END)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					--AND pr.formItemStatement = 'TEM ALGUMA ATIVIDADE PROMOCIONAL DA DIAGEO ACONTECENDO NO BAR ?'
					AND pr.[Form Visibilidade Name] = 'Actividade Promocional'
					AND pr.planResultDetailIsDeleted = 0
					AND pr.formItemOptionText <> 'DEGUSTA��O'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				)
	) AS [Atividade Promocional - Degusta��o]
	, ISNULL(
				(
					SELECT MAX(CASE WHEN pr.measureAnswer > 0 THEN 1 ELSE 0 END)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					--AND pr.formItemStatement = 'TEM ALGUMA ATIVIDADE PROMOCIONAL DA DIAGEO ACONTECENDO NO BAR ?'
					AND pr.[Form Visibilidade Name] = 'Actividade Promocional'
					AND pr.planResultDetailIsDeleted = 0
					AND pr.formItemOptionText = 'BLITZ'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				)
				, 
				0 * (
					SELECT MAX(CASE WHEN pr.measureAnswer > 0 THEN 1 ELSE 0 END)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					--AND pr.formItemStatement = 'TEM ALGUMA ATIVIDADE PROMOCIONAL DA DIAGEO ACONTECENDO NO BAR ?'
					AND pr.[Form Visibilidade Name] = 'Actividade Promocional'
					AND pr.planResultDetailIsDeleted = 0
					AND pr.formItemOptionText <> 'BLITZ'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				)
	) AS [Atividade Promocional - Blitz]
	, ISNULL(
				(
					SELECT MAX(pr.measureAnswer)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.formItemStatement = 'O BAR ESTA FUNCIONANDO ?'
					--AND pr.[Form Performance Name] = 'Bar Funcionando'
					AND pr.isCompleted = 1
					AND pr.planResultDetailIsDeleted = 0
					AND pr.formItemOptionText = 'SIM'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				)
				, 
				0 * (
					SELECT MAX(pr.measureAnswer)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.formItemStatement = 'O BAR ESTA FUNCIONANDO ?'
					--AND pr.[Form Performance Name] = 'Bar Funcionando'
					AND pr.isCompleted = 1
					AND pr.planResultDetailIsDeleted = 0
					AND pr.formItemOptionText <> 'SIM'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				)
	) AS [BAR FUNCIONANDO]
	, ISNULL(
				(
					SELECT MAX(CASE WHEN pr.measureAnswer > 0 THEN 1 ELSE 0 END)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					AND pr.formItemStatement = 'EXISTE CARTAZ FOCO DA DIAGEO IMPLEMENTADO NO BAR ?'
					--AND pr.[Form Visibilidade Name] = 'Cartaz Foco Implementado'
					AND pr.planResultDetailIsDeleted = 0
					AND pr.formItemOptionText = 'SIM'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				)
				, 
				0 * (
					SELECT MAX(CASE WHEN pr.measureAnswer > 0 THEN 1 ELSE 0 END)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					AND pr.formItemStatement = 'EXISTE CARTAZ FOCO DA DIAGEO IMPLEMENTADO NO BAR ?'
					--AND pr.[Form Visibilidade Name] = 'Cartaz Foco Implementado'
					AND pr.planResultDetailIsDeleted = 0
					AND pr.formItemOptionText <> 'SIM'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				)
	) AS [Cartaz Foco]
	, ISNULL(
				(
					SELECT MAX(CASE WHEN pr.measureAnswer > 0 THEN 1 ELSE 0 END)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					AND pr.formItemStatement = 'QUAL O MOTIVO DO BAR N�O ESTAR FUNCIONANDO ?'
					--AND pr.[Form Visibilidade Name] = 'Bar n�o funcionando'
					AND pr.planResultDetailIsDeleted = 0
					AND pr.formItemOptionText = 'FORA DO HOR�RIO DE ATENDIMENTO'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				)
				, 
				0 * (
					SELECT MAX(CASE WHEN pr.measureAnswer > 0 THEN 1 ELSE 0 END)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					AND pr.formItemStatement = 'QUAL O MOTIVO DO BAR N�O ESTAR FUNCIONANDO ?'
					--AND pr.[Form Visibilidade Name] = 'Bar n�o funcionando'
					AND pr.planResultDetailIsDeleted = 0
					AND pr.formItemOptionText <> 'FORA DO HOR�RIO DE ATENDIMENTO'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				)
	) AS [Bar n�o funcionando - FORA DO HOR�RIO DE ATENDIMENTO]
	, ISNULL(
				(
					SELECT MAX(CASE WHEN pr.measureAnswer > 0 THEN 1 ELSE 0 END)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					AND pr.formItemStatement = 'QUAL O MOTIVO DO BAR N�O ESTAR FUNCIONANDO ?'
					--AND pr.[Form Visibilidade Name] = 'Bar n�o funcionando'
					AND pr.planResultDetailIsDeleted = 0
					AND pr.formItemOptionText = 'BAR FECHOU'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				)
				, 
				0 * (
					SELECT MAX(CASE WHEN pr.measureAnswer > 0 THEN 1 ELSE 0 END)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					AND pr.formItemStatement = 'QUAL O MOTIVO DO BAR N�O ESTAR FUNCIONANDO ?'
					--AND pr.[Form Visibilidade Name] = 'Bar n�o funcionando'
					AND pr.planResultDetailIsDeleted = 0
					AND pr.formItemOptionText <> 'BAR FECHOU'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				)
	) AS [Bar n�o funcionando - BAR FECHOU]
	, ISNULL(
				(
					SELECT MAX(CASE WHEN pr.measureAnswer > 0 THEN 1 ELSE 0 END)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					AND pr.formItemStatement = 'QUAL O MOTIVO DO BAR N�O ESTAR FUNCIONANDO ?'
					--AND pr.[Form Visibilidade Name] = 'Bar n�o funcionando'
					AND pr.planResultDetailIsDeleted = 0
					AND pr.formItemOptionText = 'REFORMA'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				)
				, 
				0 * (
					SELECT MAX(CASE WHEN pr.measureAnswer > 0 THEN 1 ELSE 0 END)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					AND pr.formItemStatement = 'QUAL O MOTIVO DO BAR N�O ESTAR FUNCIONANDO ?'
					--AND pr.[Form Visibilidade Name] = 'Bar n�o funcionando'
					AND pr.planResultDetailIsDeleted = 0
					AND pr.formItemOptionText <> 'REFORMA'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				)
	) AS [Bar n�o funcionando - REFORMA]
FROM t AS l
