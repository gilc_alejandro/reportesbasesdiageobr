CREATE STATISTICS [_dta_stat_941883814_1_2_3_70] ON [DW].[Form]([formGroupId], [formItemId], [formItemOptionId], [Form Companhia Name])

SET ANSI_PADDING ON

CREATE NONCLUSTERED INDEX [_dta_index_Form_7_941883814__K68_1_2_3_8_33_70] ON [DW].[Form]
(
	[Form Participa��o de G�ndola Name] ASC
)
INCLUDE ( 	[formGroupId],
	[formItemId],
	[formItemOptionId],
	[formItemStatement],
	[Form Template Form Type Code],
	[Form Companhia Name]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [FG_Data1]

SET ANSI_PADDING ON

CREATE NONCLUSTERED INDEX [_dta_index_Form_7_941883814__K68_1_2_3_8_33_38_40_66_67_70] ON [DW].[Form]
(
	[Form Participa��o de G�ndola Name] ASC
)
INCLUDE ( 	[formGroupId],
	[formItemId],
	[formItemOptionId],
	[formItemStatement],
	[Form Template Form Type Code],
	[Form Sales Drivers Name],
	[Form Product List Name],
	[Form Visibilidade Name],
	[Form Planograma Name],
	[Form Companhia Name]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [FG_Data1]

CREATE STATISTICS [_dta_stat_1133884498_3_2_20_23_17_22] ON [DW].[PlanResult]([locationId], [devicePlanId], [formAnswerValue], [planResultDetailIsDeleted], [dateAudited], [measureAnswer])

CREATE STATISTICS [_dta_stat_1133884498_3_2_17_23_22] ON [DW].[PlanResult]([locationId], [devicePlanId], [dateAudited], [planResultDetailIsDeleted], [measureAnswer])

CREATE STATISTICS [_dta_stat_1133884498_8_3_2_4_5] ON [DW].[PlanResult]([isCompleted], [locationId], [devicePlanId], [formGroupId], [formItemId])

CREATE STATISTICS [_dta_stat_1133884498_3_20_23_17_22] ON [DW].[PlanResult]([locationId], [formAnswerValue], [planResultDetailIsDeleted], [dateAudited], [measureAnswer])

CREATE STATISTICS [_dta_stat_1133884498_17_23_22] ON [DW].[PlanResult]([dateAudited], [planResultDetailIsDeleted], [measureAnswer])

SET ANSI_PADDING ON

CREATE NONCLUSTERED INDEX [_dta_index_PlanResult_7_1133884498__K23_K3_K2_K8_K4_K5_K6_K17_K20_K22] ON [DW].[PlanResult]
(
	[planResultDetailIsDeleted] ASC,
	[locationId] ASC,
	[devicePlanId] ASC,
	[isCompleted] ASC,
	[formGroupId] ASC,
	[formItemId] ASC,
	[formItemOptionId] ASC,
	[dateAudited] ASC,
	[formAnswerValue] ASC,
	[measureAnswer] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [FG_Data1]

CREATE STATISTICS [_dta_stat_1133884498_3_17_23_22] ON [DW].[PlanResult]([locationId], [dateAudited], [planResultDetailIsDeleted], [measureAnswer])

CREATE STATISTICS [_dta_stat_1133884498_22_3_2_8_4_5_6_17] ON [DW].[PlanResult]([measureAnswer], [locationId], [devicePlanId], [isCompleted], [formGroupId], [formItemId], [formItemOptionId], [dateAudited])

CREATE STATISTICS [_dta_stat_941883814_1_2_3_67] ON [DW].[Form]([formGroupId], [formItemId], [formItemOptionId], [Form Planograma Name])

SET ANSI_PADDING ON

CREATE NONCLUSTERED INDEX [_dta_index_Form_7_941883814__K40_1_2_3_8_33] ON [DW].[Form]
(
	[Form Product List Name] ASC
)
INCLUDE ( 	[formGroupId],
	[formItemId],
	[formItemOptionId],
	[formItemStatement],
	[Form Template Form Type Code]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [FG_Data1]

CREATE STATISTICS [_dta_stat_1133884498_3_8_17_22] ON [DW].[PlanResult]([locationId], [isCompleted], [dateAudited], [measureAnswer])

CREATE STATISTICS [_dta_stat_1133884498_4_5_6_20_8_17_22] ON [DW].[PlanResult]([formGroupId], [formItemId], [formItemOptionId], [formAnswerValue], [isCompleted], [dateAudited], [measureAnswer])

CREATE STATISTICS [_dta_stat_1133884498_4_5_6_8_17_22] ON [DW].[PlanResult]([formGroupId], [formItemId], [formItemOptionId], [isCompleted], [dateAudited], [measureAnswer])

CREATE STATISTICS [_dta_stat_1133884498_3_4_5_6_2_20_23_8] ON [DW].[PlanResult]([locationId], [formGroupId], [formItemId], [formItemOptionId], [devicePlanId], [formAnswerValue], [planResultDetailIsDeleted], [isCompleted])

SET ANSI_PADDING ON

CREATE NONCLUSTERED INDEX [_dta_index_PlanResult_7_1133884498__K23_K3_K4_K5_K6_K20_K8_K17_K2_K22] ON [DW].[PlanResult]
(
	[planResultDetailIsDeleted] ASC,
	[locationId] ASC,
	[formGroupId] ASC,
	[formItemId] ASC,
	[formItemOptionId] ASC,
	[formAnswerValue] ASC,
	[isCompleted] ASC,
	[dateAudited] ASC,
	[devicePlanId] ASC,
	[measureAnswer] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [FG_Data1]

CREATE STATISTICS [_dta_stat_1133884498_2_3_20_8_17_22] ON [DW].[PlanResult]([devicePlanId], [locationId], [formAnswerValue], [isCompleted], [dateAudited], [measureAnswer])

CREATE STATISTICS [_dta_stat_1133884498_22_3_4_5_6_20_23_8] ON [DW].[PlanResult]([measureAnswer], [locationId], [formGroupId], [formItemId], [formItemOptionId], [formAnswerValue], [planResultDetailIsDeleted], [isCompleted])

