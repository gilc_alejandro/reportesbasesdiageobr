
DECLARE @startDate Datetime = '2017-12-17', @endDate Datetime = '2017-12-23'
DECLARE @formTypeCode Varchar(10) = 'IA' --EX, IA

;WITH t 
AS
(
	select lc.locationName AS PDV
	, lc.locationReference AS CNPJ
	, lc.locationId
	, Products.[Form Product List Name] AS [Codigo Produto]
	, Products.[Form Product List Category] AS [Categoria Producto]
	, Products.[Form Product List Standard Name] AS [Descripcion Producto]
	, DATENAME(MONTH, @endDate)+'/'+CAST(YEAR(@endDate) AS VARCHAR(4)) AS M�s
		, CAST(DAY(@startDate) AS VARCHAR(2))+'/'+CAST(MONTH(@startDate) AS VARCHAR(2))+' a '+CAST(DAY(@endDate) AS VARCHAR(2))+'/'+CAST(MONTH(@endDate) AS VARCHAR(2)) AS Semana
	, @formTypeCode AS [Form Type Code]
	from [DW].LocationsCashCarryNoviembre as l
	inner join [DW].Location as lc on l.CNPJ = lc.locationReference
	inner join
	(
			select pl.*
			from [DW].ProductosCashCarryNew as p
			inner join 
			(
					select av.attributeValue as [Form Product List Name], 
					(
						select av2.attributeValue
						from [System].tbl_Attributes_Values as av2
						where av2.categoryItemId = av.categoryItemId and av2.attributeId = 94
					) as [Form Product List Category], 
					(
						select av2.attributeValue
						from [System].tbl_Attributes_Values as av2
						where av2.categoryItemId = av.categoryItemId and av2.attributeId = 99
					) as [Form Product List Standard Name]
					from [System].tbl_Category_Items as ci
					inner join [System].tbl_Attributes_Values as av on ci.categoryItemId = av.categoryItemId
					where ci.categoryId = 51 and av.attributeId = 91

			) as pl on p.Codigo = pl. [Form Product List Name]

	) as Products on 1 = 1
)


SELECT l.*
	/*,(
		SELECT MAX(pr.auditedBy)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.[Form Product List Name] = l.[Codigo Produto]
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					AND pr.planResultDetailIsDeleted = 0
					AND pr.[Form Template Form Type Code] = @formTypeCode
	) AS auditedBy*/
	, COALESCE(
				CAST(
				(
					SELECT MAX(pr.measureAnswer)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.[Form Product List Name] = l.[Codigo Produto]
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.[Form Sales Drivers Name] = 'DISTRIBUI��O'
					AND pr.isCompleted = 1
					AND pr.planResultDetailIsDeleted = 0
					AND pr.formItemOptionText = 'SIM'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				) AS VARCHAR(50))
				, CAST(
				0 * (
					SELECT MAX(pr.measureAnswer)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.[Form Product List Name] = l.[Codigo Produto]
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.[Form Sales Drivers Name] = 'DISTRIBUI��O'
					AND pr.isCompleted = 1
					AND pr.planResultDetailIsDeleted = 0
					AND pr.formItemOptionText <> 'SIM'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				) AS VARCHAR(50))
				, 'NA'
	) AS [Presen�a]
	, COALESCE(
		CAST(
			(
				SELECT AVG(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId 
				AND pr.[Form Product List Name] = l.[Codigo Produto]
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.[Form Sales Drivers Name] = 'PRE�O'
				AND pr.isCompleted = 1
				AND pr.measureAnswer > 0
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
		AS VARCHAR(50))
		, 'NA'
		) AS [Pre�o M�dio]
	, COALESCE(
		CAST(
		(
			SELECT MAX(CASE WHEN pr.measureAnswer > 0 THEN 1 ELSE 0 END)
			FROM [DW].vw_PRDFIO AS pr
			WHERE pr.locationId = l.locationId 
			AND pr.dateAudited BETWEEN @startDate AND @endDate
			AND pr.isCompleted = 1
			AND pr.[Form Visibilidade Name] = 'Ponto Extra'
			AND pr.planResultDetailIsDeleted = 0
			AND pr.[Form Template Form Type Code] = @formTypeCode
			AND pr.auditedBy NOT LIKE '%test%'
		) AS VARCHAR(50))
		, 'NA'
	) AS [Ponto Extra (%)]
FROM t AS l
