
DECLARE @startDate Datetime = '2017-12-17', @endDate Datetime = '2017-12-23'
DECLARE @formTypeCode Varchar(10) = 'IA' --EX, IA para IA solo interesa presencia y precio, comentar las otras columnas al ejecutar IA para su rapida ejecucion.

;WITH t 
AS
(
	select lc.locationName AS PDV
	, lc.locationReference AS CNPJ
	, lc.locationId
	, Products.[Form Product List Name] AS [Codigo Produto]
	, Products.[Form Product List Category] AS [Categoria Producto]
	, Products.[Form Product List Standard Name] AS [Descripcion Producto]
	, DATENAME(MONTH, @endDate)+'/'+CAST(YEAR(@endDate) AS VARCHAR(4)) AS M�s
		, CAST(DAY(@startDate) AS VARCHAR(2))+'/'+CAST(MONTH(@startDate) AS VARCHAR(2))+' a '+CAST(DAY(@endDate) AS VARCHAR(2))+'/'+CAST(MONTH(@endDate) AS VARCHAR(2)) AS Semana
	, @formTypeCode AS [Form Type Code]
	from [DW].LocationsKATRNoviembre as l
	inner join [DW].Location as lc on l.CNPJ = lc.locationReference
	inner join
	(
			select pl.*
			from [DW].ProductosKATRNew as p
			inner join 
			(
					select av.attributeValue as [Form Product List Name], 
					(
						select av2.attributeValue
						from [System].tbl_Attributes_Values as av2
						where av2.categoryItemId = av.categoryItemId and av2.attributeId = 94
					) as [Form Product List Category], 
					(
						select av2.attributeValue
						from [System].tbl_Attributes_Values as av2
						where av2.categoryItemId = av.categoryItemId and av2.attributeId = 99
					) as [Form Product List Standard Name]
					from [System].tbl_Category_Items as ci
					inner join [System].tbl_Attributes_Values as av on ci.categoryItemId = av.categoryItemId
					where ci.categoryId = 51 and av.attributeId = 91

			) as pl on p.Codigo = pl. [Form Product List Name]

	) as Products on 1 = 1
)

SELECT l.*
	, COALESCE(
				(
					SELECT MAX(pr.measureAnswer)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.[Form Product List Name] = l.[Codigo Produto]
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.[Form Sales Drivers Name] = 'DISTRIBUI��O'
					AND pr.isCompleted = 1
					AND pr.planResultDetailIsDeleted = 0
					AND pr.formItemOptionText = 'SIM'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				)
				,
				0 * (
					SELECT MAX(pr.measureAnswer)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.[Form Product List Name] = l.[Codigo Produto]
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.[Form Sales Drivers Name] = 'DISTRIBUI��O'
					AND pr.isCompleted = 1
					AND pr.planResultDetailIsDeleted = 0
					AND pr.formItemOptionText <> 'SIM'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				)
	) AS [Presen�a]
	,
		(
			SELECT AVG(pr.measureAnswer)
			FROM [DW].vw_PRDFIO AS pr
			WHERE pr.locationId = l.locationId 
			AND pr.[Form Product List Name] = l.[Codigo Produto]
			AND pr.dateAudited BETWEEN @startDate AND @endDate
			AND pr.[Form Sales Drivers Name] = 'PRE�O'
			AND pr.isCompleted = 1
			AND pr.planResultDetailIsDeleted = 0
			AND pr.[Form Template Form Type Code] = @formTypeCode
			AND pr.auditedBy NOT LIKE '%test%'
	) AS [Pre�o M�dio]
	,
		(
			SELECT MAX(pr.measureAnswer)
			FROM [DW].vw_PRDFIO AS pr
			WHERE l.locationId = pr.locationId
			AND pr.dateAudited BETWEEN @startDate AND @endDate
			AND pr.isCompleted = 1
			AND pr.[Form Visibilidade Name] = 'Ponto Extra'
			AND pr.planResultDetailIsDeleted = 0
			AND pr.[Form Template Form Type Code] = @formTypeCode
			AND pr.auditedBy NOT LIKE '%test%'
	) AS [Ponto Extra Diageo/PDV]
	, 
		(
			SELECT CASE WHEN (SUM(CASE WHEN [Form Companhia Name] IS NULL THEN pr.measureAnswer ELSE 0 END)) = 0 THEN NULL
					ELSE
						SUM(CASE WHEN [Form Companhia Name] IS NULL THEN 0 ELSE pr.measureAnswer END)
						/
						SUM(CASE WHEN [Form Companhia Name] IS NULL THEN pr.measureAnswer ELSE 0 END)
					END
			FROM [DW].vw_PRDFIO AS pr
			WHERE pr.locationId = l.locationId 
			AND pr.[Form Participa��o de G�ndola Name] = l.[Categoria Producto]
			AND pr.dateAudited BETWEEN @startDate AND @endDate
			AND pr.isCompleted = 1
			AND pr.[Form Participa��o de G�ndola Name] IS NOT NULL
			--AND pr.formItemStatement != 'INFORME O N�MERO DE FRENTES DO PRODUTO CATUABA 300ml'
			AND pr.[Form No Frente Diageo Name] IS NULL
			AND (pr.[Form Companhia Name] IS NULL OR pr.[Form Companhia Name] NOT IN ('SKOL BEATS', 'SM X1'))
			AND pr.planResultDetailIsDeleted = 0
			AND pr.[Form Template Form Type Code] = @formTypeCode
			AND pr.auditedBy NOT LIKE '%test%'
	) AS [Facing (%)]
	,
		(
			SELECT SUM(CASE WHEN [Form Companhia Name] IS NULL THEN 0 ELSE pr.measureAnswer END)
			FROM [DW].vw_PRDFIO AS pr
			WHERE pr.locationId = l.locationId 
			AND pr.[Form Participa��o de G�ndola Name] = l.[Categoria Producto]
			AND pr.dateAudited BETWEEN @startDate AND @endDate
			AND pr.isCompleted = 1
			AND pr.[Form Participa��o de G�ndola Name] IS NOT NULL
			--AND pr.formItemStatement != 'INFORME O N�MERO DE FRENTES DO PRODUTO CATUABA 300ml'
			AND pr.[Form No Frente Diageo Name] IS NULL
			AND (pr.[Form Companhia Name] IS NULL OR pr.[Form Companhia Name] NOT IN ('SKOL BEATS', 'SM X1'))
			AND pr.planResultDetailIsDeleted = 0
			AND pr.[Form Template Form Type Code] = @formTypeCode
			AND pr.auditedBy NOT LIKE '%test%'
	) AS [Frentes Diageo]
	, 
		(
			SELECT SUM(pr.measureAnswer/*CASE WHEN [Form Participa��o de G�ndola Name] IS NULL THEN 0 ELSE pr.measureAnswer END*/)
			FROM [DW].vw_PRDFIO AS pr
			WHERE pr.locationId = l.locationId 
			AND pr.[Form Product List Name] = l.[Codigo Produto]
			AND pr.dateAudited BETWEEN @startDate AND @endDate
			AND pr.isCompleted = 1
			AND pr.[Form Visibilidade Name] = 'FRENTE PRODUCTOS'
			AND pr.planResultDetailIsDeleted = 0
			AND pr.[Form Template Form Type Code] = @formTypeCode
			AND pr.auditedBy NOT LIKE '%test%'
	) AS [Frentes Produtos]
	, 
		(
			SELECT SUM(CASE WHEN [Form Companhia Name] IS NULL THEN pr.measureAnswer ELSE 0 END)
			FROM [DW].vw_PRDFIO AS pr
			WHERE pr.locationId = l.locationId 
			AND pr.[Form Participa��o de G�ndola Name] = l.[Categoria Producto]
			AND pr.dateAudited BETWEEN @startDate AND @endDate
			AND pr.isCompleted = 1
			AND pr.[Form Participa��o de G�ndola Name] IS NOT NULL
			--AND pr.formItemStatement != 'INFORME O N�MERO DE FRENTES DO PRODUTO CATUABA 300ml'
			AND pr.[Form No Frente Diageo Name] IS NULL
			AND (pr.[Form Companhia Name] IS NULL OR pr.[Form Companhia Name] NOT IN ('SKOL BEATS', 'SM X1'))
			AND pr.planResultDetailIsDeleted = 0
			AND pr.[Form Template Form Type Code] = @formTypeCode
			AND pr.auditedBy NOT LIKE '%test%'
	) AS [Frentes Total]
	, COALESCE(
				(
					SELECT MAX(pr.measureAnswer)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					AND pr.formItemOptionText = 'SIM'
					AND pr.[Form Planograma Name] = 'Planograma'
					AND pr.planResultDetailIsDeleted = 0
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				)
				,
				0 * (
					SELECT MAX(pr.measureAnswer)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					AND pr.formItemOptionText <> 'SIM'
					AND pr.[Form Planograma Name] = 'Planograma'
					AND pr.planResultDetailIsDeleted = 0
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				)
	) AS [% Planograma]
	, COALESCE(
			(
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement = 'O PDV TEM DISPLAY JW IMPLEMENTADO?'
				AND pr.[Form Visibilidade Name] = 'Display JW'
				AND pr.formItemOptionText = 'SIM'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
			,
			0 * (
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement = 'O PDV TEM DISPLAY JW IMPLEMENTADO?'
				AND pr.[Form Visibilidade Name] = 'Display JW'
				AND pr.formItemOptionText <> 'SIM'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
	) AS [DISPLAY JW]
	, COALESCE(
			(
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement in ('QUAIS MARCAS EST�O IMPLEMENTADAS NO DISPLAY JW?', 'QUAIS AS MARCAS EST�O IMPLEMENTADAS NO DISPLAY JW?')
				AND pr.[Form Visibilidade Name] = 'Display JW - Marcas'
				AND pr.formItemOptionText = 'JW RED LABEL 1L'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
			,
			0 * (
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement in ('QUAIS MARCAS EST�O IMPLEMENTADAS NO DISPLAY JW?', 'QUAIS AS MARCAS EST�O IMPLEMENTADAS NO DISPLAY JW?')
				AND pr.[Form Visibilidade Name] = 'Display JW - Marcas'
				AND pr.formItemOptionText <> 'JW RED LABEL 1L'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			) 
	) AS [DISPLAY JW - JW RED LABEL 1L]
	, COALESCE(
			(
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement in ('QUAIS MARCAS EST�O IMPLEMENTADAS NO DISPLAY JW?', 'QUAIS AS MARCAS EST�O IMPLEMENTADAS NO DISPLAY JW?')
				AND pr.[Form Visibilidade Name] = 'Display JW - Marcas'
				AND pr.formItemOptionText = 'JW RED LABEL 750ml'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
			,
			0 * (
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement in ('QUAIS MARCAS EST�O IMPLEMENTADAS NO DISPLAY JW?', 'QUAIS AS MARCAS EST�O IMPLEMENTADAS NO DISPLAY JW?')
				AND pr.[Form Visibilidade Name] = 'Display JW - Marcas'
				AND pr.formItemOptionText <> 'JW RED LABEL 750ml'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
	) AS [DISPLAY JW - JW RED LABEL 750ml]
	, COALESCE(
			(
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement in ('QUAIS MARCAS EST�O IMPLEMENTADAS NO DISPLAY JW?', 'QUAIS AS MARCAS EST�O IMPLEMENTADAS NO DISPLAY JW?')
				AND pr.[Form Visibilidade Name] = 'Display JW - Marcas'
				AND pr.formItemOptionText = 'JW BLACK LABEL 1L'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
			,
			0 * (
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement in ('QUAIS MARCAS EST�O IMPLEMENTADAS NO DISPLAY JW?', 'QUAIS AS MARCAS EST�O IMPLEMENTADAS NO DISPLAY JW?')
				AND pr.[Form Visibilidade Name] = 'Display JW - Marcas'
				AND pr.formItemOptionText <> 'JW BLACK LABEL 1L'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
	) AS [DISPLAY JW - JW BLACK LABEL 1L]
	, COALESCE(
			(
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement in ('QUAIS MARCAS EST�O IMPLEMENTADAS NO DISPLAY JW?', 'QUAIS AS MARCAS EST�O IMPLEMENTADAS NO DISPLAY JW?')
				AND pr.[Form Visibilidade Name] = 'Display JW - Marcas'
				AND pr.formItemOptionText = 'JW BLACK LABEL 750ml'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
			,
			0 * (
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement in ('QUAIS MARCAS EST�O IMPLEMENTADAS NO DISPLAY JW?', 'QUAIS AS MARCAS EST�O IMPLEMENTADAS NO DISPLAY JW?')
				AND pr.[Form Visibilidade Name] = 'Display JW - Marcas'
				AND pr.formItemOptionText <> 'JW BLACK LABEL 750ml'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
	) AS [DISPLAY JW - JW BLACK LABEL 750ml]
	, COALESCE(
			(
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement in ('QUAIS MARCAS EST�O IMPLEMENTADAS NO DISPLAY JW?', 'QUAIS AS MARCAS EST�O IMPLEMENTADAS NO DISPLAY JW?')
				AND pr.[Form Visibilidade Name] = 'Display JW - Marcas'
				AND pr.formItemOptionText = 'JW RED RYE'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
			,
			0 * (
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement in ('QUAIS MARCAS EST�O IMPLEMENTADAS NO DISPLAY JW?', 'QUAIS AS MARCAS EST�O IMPLEMENTADAS NO DISPLAY JW?')
				AND pr.[Form Visibilidade Name] = 'Display JW - Marcas'
				AND pr.formItemOptionText <> 'JW RED RYE'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
	) AS [DISPLAY JW - JW RED RYE]
	, COALESCE(
			(
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement in ('QUAIS MARCAS EST�O IMPLEMENTADAS NO DISPLAY JW?', 'QUAIS AS MARCAS EST�O IMPLEMENTADAS NO DISPLAY JW?')
				AND pr.[Form Visibilidade Name] = 'Display JW - Marcas'
				AND pr.formItemOptionText = 'JW GOLD 750ml'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
			,
			0 * (
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement in ('QUAIS MARCAS EST�O IMPLEMENTADAS NO DISPLAY JW?', 'QUAIS AS MARCAS EST�O IMPLEMENTADAS NO DISPLAY JW?')
				AND pr.[Form Visibilidade Name] = 'Display JW - Marcas'
				AND pr.formItemOptionText <> 'JW GOLD 750ml'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
	) AS [DISPLAY JW - JW GOLD 750ml]
	, COALESCE(
			(
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement in ('QUAIS MARCAS EST�O IMPLEMENTADAS NO DISPLAY JW?', 'QUAIS AS MARCAS EST�O IMPLEMENTADAS NO DISPLAY JW?')
				AND pr.[Form Visibilidade Name] = 'Display JW - Marcas'
				AND pr.formItemOptionText = 'OUTROS'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
			,
			0 * (
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement in ('QUAIS MARCAS EST�O IMPLEMENTADAS NO DISPLAY JW?', 'QUAIS AS MARCAS EST�O IMPLEMENTADAS NO DISPLAY JW?')
				AND pr.[Form Visibilidade Name] = 'Display JW - Marcas'
				AND pr.formItemOptionText <> 'OUTROS'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
	) AS [DISPLAY JW - OUTROS]
	, COALESCE(
			(
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement = 'QUAL A LOCALZA��O DO DISPLAY JW IMPLEMENTADO?'
				AND pr.[Form Visibilidade Name] = 'Display JW - LOCALZA��O'
				AND pr.formItemOptionText = 'CORREDOR DIRETOR'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
			,
			0 * (
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement = 'QUAL A LOCALZA��O DO DISPLAY JW IMPLEMENTADO?'
				AND pr.[Form Visibilidade Name] = 'Display JW - LOCALZA��O'
				AND pr.formItemOptionText <> 'CORREDOR DIRETOR'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
	) AS [DISPLAY JW - CORREDOR DIRETOR]
	, COALESCE(
			(
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement = 'QUAL A LOCALZA��O DO DISPLAY JW IMPLEMENTADO?'
				AND pr.[Form Visibilidade Name] = 'Display JW - LOCALZA��O'
				AND pr.formItemOptionText = 'CORREDOR CATEGORIA'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
			,
			0 * (
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement = 'QUAL A LOCALZA��O DO DISPLAY JW IMPLEMENTADO?'
				AND pr.[Form Visibilidade Name] = 'Display JW - LOCALZA��O'
				AND pr.formItemOptionText <> 'CORREDOR CATEGORIA'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
	) AS [DISPLAY JW - CORREDOR CATEGORIA]
	, COALESCE(
			(
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement = 'QUAL A LOCALZA��O DO DISPLAY JW IMPLEMENTADO?'
				AND pr.[Form Visibilidade Name] = 'Display JW - LOCALZA��O'
				AND pr.formItemOptionText = 'CORREDOR CERVEJA'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
			,
			0 * (
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement = 'QUAL A LOCALZA��O DO DISPLAY JW IMPLEMENTADO?'
				AND pr.[Form Visibilidade Name] = 'Display JW - LOCALZA��O'
				AND pr.formItemOptionText <> 'CORREDOR CERVEJA'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
	) AS [DISPLAY JW - CORREDOR CERVEJA]
	, COALESCE(
			(
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement = 'QUAL A LOCALZA��O DO DISPLAY JW IMPLEMENTADO?'
				AND pr.[Form Visibilidade Name] = 'Display JW - LOCALZA��O'
				AND pr.formItemOptionText = 'CANTINHO CHURRASCO'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
			,
			0 * (
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement = 'QUAL A LOCALZA��O DO DISPLAY JW IMPLEMENTADO?'
				AND pr.[Form Visibilidade Name] = 'Display JW - LOCALZA��O'
				AND pr.formItemOptionText <> 'CANTINHO CHURRASCO'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
	) AS [DISPLAY JW - CANTINHO CHURRASCO]
	, COALESCE(
			(
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement = 'QUAL A LOCALZA��O DO DISPLAY JW IMPLEMENTADO?'
				AND pr.[Form Visibilidade Name] = 'Display JW - LOCALZA��O'
				AND pr.formItemOptionText = 'OUTROS'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
			,
			0 * (
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement = 'QUAL A LOCALZA��O DO DISPLAY JW IMPLEMENTADO?'
				AND pr.[Form Visibilidade Name] = 'Display JW - LOCALZA��O'
				AND pr.formItemOptionText <> 'OUTROS'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
	) AS [DISPLAY JW - OUTROS]
	, COALESCE(
			(
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement = 'O PDV TEM DISPLAY BLACK & WHITE IMPLEMENTADO?'
				AND pr.[Form Visibilidade Name] = 'Display BW'
				AND pr.formItemOptionText = 'SIM'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
			,
			0 * (
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement = 'O PDV TEM DISPLAY BLACK & WHITE IMPLEMENTADO?'
				AND pr.[Form Visibilidade Name] = 'Display BW'
				AND pr.formItemOptionText <> 'SIM'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
	) AS [Display B&W]
	, COALESCE(
			(
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement = 'QUAL A LOCALZA��O DO DISPLAY BLACK & WHITE IMPLEMENTADO?'
				AND pr.[Form Visibilidade Name] = 'Display BW - LOCALZA��O'
				AND pr.formItemOptionText = 'CORREDOR DIRETOR'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
			, CAST(
			0 * (
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement = 'QUAL A LOCALZA��O DO DISPLAY BLACK & WHITE IMPLEMENTADO?'
				AND pr.[Form Visibilidade Name] = 'Display BW - LOCALZA��O'
				AND pr.formItemOptionText <> 'CORREDOR DIRETOR'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			) AS VARCHAR(50))
	) AS [Display B&W - Corredor Diretor]
	, COALESCE(
			(
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement = 'QUAL A LOCALZA��O DO DISPLAY BLACK & WHITE IMPLEMENTADO?'
				AND pr.[Form Visibilidade Name] = 'Display BW - LOCALZA��O'
				AND pr.formItemOptionText = 'CORREDOR CATEGORIA'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
			,
			0 * (
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement = 'QUAL A LOCALZA��O DO DISPLAY BLACK & WHITE IMPLEMENTADO?'
				AND pr.[Form Visibilidade Name] = 'Display BW - LOCALZA��O'
				AND pr.formItemOptionText <> 'CORREDOR CATEGORIA'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
	) AS [Display B&W - Corredor Categoria]
	, COALESCE(
			(
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement = 'QUAL A LOCALZA��O DO DISPLAY BLACK & WHITE IMPLEMENTADO?'
				AND pr.[Form Visibilidade Name] = 'Display BW - LOCALZA��O'
				AND pr.formItemOptionText = 'CORREDOR CERVEJA'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
			,
			0 * (
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement = 'QUAL A LOCALZA��O DO DISPLAY BLACK & WHITE IMPLEMENTADO?'
				AND pr.[Form Visibilidade Name] = 'Display BW - LOCALZA��O'
				AND pr.formItemOptionText <> 'CORREDOR CERVEJA'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
	) AS [Display B&W - Corredor Cerveja]
	, COALESCE(
			(
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement = 'QUAL A LOCALZA��O DO DISPLAY BLACK & WHITE IMPLEMENTADO?'
				AND pr.[Form Visibilidade Name] = 'Display BW - LOCALZA��O'
				AND pr.formItemOptionText = 'CANTINHO CHURRASCO'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
			,
			0 * (
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement = 'QUAL A LOCALZA��O DO DISPLAY BLACK & WHITE IMPLEMENTADO?'
				AND pr.[Form Visibilidade Name] = 'Display BW - LOCALZA��O'
				AND pr.formItemOptionText <> 'CANTINHO CHURRASCO'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
	) AS [Display B&W - Cantinho Churrasco]
	, COALESCE(
			(
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement = 'QUAL A LOCALZA��O DO DISPLAY BLACK & WHITE IMPLEMENTADO?'
				AND pr.[Form Visibilidade Name] = 'Display BW - LOCALZA��O'
				AND pr.formItemOptionText = 'OUTROS'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
			,
			0 * (
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement = 'QUAL A LOCALZA��O DO DISPLAY BLACK & WHITE IMPLEMENTADO?'
				AND pr.[Form Visibilidade Name] = 'Display BW - LOCALZA��O'
				AND pr.formItemOptionText <> 'OUTROS'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
	) AS [Display B&W - Outros]
	, COALESCE(
			(
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement = 'O PDV TEM PONTO EXTRA DE ILHA DE NATAL IMPLEMENTADO?'
				AND pr.[Form Visibilidade Name] = 'Ilha de Natal'
				AND pr.formItemOptionText = 'SIM'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
			,
			0 * (
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement = 'O PDV TEM PONTO EXTRA DE ILHA DE NATAL IMPLEMENTADO?'
				AND pr.[Form Visibilidade Name] = 'Ilha de Natal'
				AND pr.formItemOptionText <> 'SIM'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
	) AS [Ilha de Natal]
	, COALESCE(
			(
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement = 'QUAIS MARCAS EST�O IMPLEMENTADAS NA ILHA DE NATAL?'
				AND pr.[Form Visibilidade Name] = 'Ilha de Natal - Marcas'
				AND pr.formItemOptionText = 'JW RED LABEL 1L'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
			,
			0 * (
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement = 'QUAIS MARCAS EST�O IMPLEMENTADAS NA ILHA DE NATAL?'
				AND pr.[Form Visibilidade Name] = 'Ilha de Natal - Marcas'
				AND pr.formItemOptionText <> 'JW RED LABEL 1L'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
	) AS [Ilha de Natal - JW RED LABEL 1L]
	, COALESCE(
			(
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement = 'QUAIS MARCAS EST�O IMPLEMENTADAS NA ILHA DE NATAL?'
				AND pr.[Form Visibilidade Name] = 'Ilha de Natal - Marcas'
				AND pr.formItemOptionText = 'JW RED LABEL 750ml'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
			,
			0 * (
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement = 'QUAIS MARCAS EST�O IMPLEMENTADAS NA ILHA DE NATAL?'
				AND pr.[Form Visibilidade Name] = 'Ilha de Natal - Marcas'
				AND pr.formItemOptionText <> 'JW RED LABEL 750ml'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
	) AS [Ilha de Natal - JW RED LABEL 750ml]
	, COALESCE(
			(
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement = 'QUAIS MARCAS EST�O IMPLEMENTADAS NA ILHA DE NATAL?'
				AND pr.[Form Visibilidade Name] = 'Ilha de Natal - Marcas'
				AND pr.formItemOptionText = 'JW BLACK LABEL 1L'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
			,
			0 * (
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement = 'QUAIS MARCAS EST�O IMPLEMENTADAS NA ILHA DE NATAL?'
				AND pr.[Form Visibilidade Name] = 'Ilha de Natal - Marcas'
				AND pr.formItemOptionText <> 'JW BLACK LABEL 1L'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
	) AS [Ilha de Natal - JW BLACK LABEL 1L]
	, COALESCE(
			(
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement = 'QUAIS MARCAS EST�O IMPLEMENTADAS NA ILHA DE NATAL?'
				AND pr.[Form Visibilidade Name] = 'Ilha de Natal - Marcas'
				AND pr.formItemOptionText = 'JW BLACK LABEL 750ml'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
			,
			0 * (
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement = 'QUAIS MARCAS EST�O IMPLEMENTADAS NA ILHA DE NATAL?'
				AND pr.[Form Visibilidade Name] = 'Ilha de Natal - Marcas'
				AND pr.formItemOptionText <> 'JW BLACK LABEL 750ml'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
	) AS [Ilha de Natal - JW BLACK LABEL 750ml]
	, COALESCE(
			(
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement = 'QUAIS MARCAS EST�O IMPLEMENTADAS NA ILHA DE NATAL?'
				AND pr.[Form Visibilidade Name] = 'Ilha de Natal - Marcas'
				AND pr.formItemOptionText = 'JW RED RYE'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
			,
			0 * (
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement = 'QUAIS MARCAS EST�O IMPLEMENTADAS NA ILHA DE NATAL?'
				AND pr.[Form Visibilidade Name] = 'Ilha de Natal - Marcas'
				AND pr.formItemOptionText <> 'JW RED RYE'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
	) AS [Ilha de Natal - JW RED RYE]
	, COALESCE(
			(
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement = 'QUAIS MARCAS EST�O IMPLEMENTADAS NA ILHA DE NATAL?'
				AND pr.[Form Visibilidade Name] = 'Ilha de Natal - Marcas'
				AND pr.formItemOptionText = 'JW GOLD 750ml'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
			,
			0 * (
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement = 'QUAIS MARCAS EST�O IMPLEMENTADAS NA ILHA DE NATAL?'
				AND pr.[Form Visibilidade Name] = 'Ilha de Natal - Marcas'
				AND pr.formItemOptionText <> 'JW GOLD 750ml'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
	) AS [Ilha de Natal - JW GOLD 750ml]
	, COALESCE(
			(
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement = 'QUAIS MARCAS EST�O IMPLEMENTADAS NA ILHA DE NATAL?'
				AND pr.[Form Visibilidade Name] = 'Ilha de Natal - Marcas'
				AND pr.formItemOptionText = 'OUTROS'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
			,
			0 * (
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement = 'QUAIS MARCAS EST�O IMPLEMENTADAS NA ILHA DE NATAL?'
				AND pr.[Form Visibilidade Name] = 'Ilha de Natal - Marcas'
				AND pr.formItemOptionText <> 'OUTROS'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
	) AS [Ilha de Natal - OUTROS]
	, COALESCE(
			(
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement = 'QUAL A LOCALZA��O DA ILHA DE NATAL IMPLEMENTADO?'
				AND pr.[Form Visibilidade Name] = 'Ilha de Natal - LOCALZA��O'
				AND pr.formItemOptionText = 'CORREDOR DIRETOR'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
			,
			0 * (
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement = 'QUAL A LOCALZA��O DA ILHA DE NATAL IMPLEMENTADO?'
				AND pr.[Form Visibilidade Name] = 'Ilha de Natal - LOCALZA��O'
				AND pr.formItemOptionText <> 'CORREDOR DIRETOR'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
	) AS [Ilha de Natal - Corredor Diretor]
	, COALESCE(
			(
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement = 'QUAL A LOCALZA��O DA ILHA DE NATAL IMPLEMENTADO?'
				AND pr.[Form Visibilidade Name] = 'Ilha de Natal - LOCALZA��O'
				AND pr.formItemOptionText = 'CORREDOR CATEGORIA'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
			,
			0 * (
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement = 'QUAL A LOCALZA��O DA ILHA DE NATAL IMPLEMENTADO?'
				AND pr.[Form Visibilidade Name] = 'Ilha de Natal - LOCALZA��O'
				AND pr.formItemOptionText <> 'CORREDOR CATEGORIA'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
	) AS [Ilha de Natal - Corredor Categoria]
	, COALESCE(
			(
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement = 'QUAL A LOCALZA��O DA ILHA DE NATAL IMPLEMENTADO?'
				AND pr.[Form Visibilidade Name] = 'Ilha de Natal - LOCALZA��O'
				AND pr.formItemOptionText = 'CORREDOR CERVEJA'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
			,
			0 * (
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement = 'QUAL A LOCALZA��O DA ILHA DE NATAL IMPLEMENTADO?'
				AND pr.[Form Visibilidade Name] = 'Ilha de Natal - LOCALZA��O'
				AND pr.formItemOptionText <> 'CORREDOR CERVEJA'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
	) AS [Ilha de Natal - Corredor Cerveja]
	, COALESCE(
			(
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement = 'QUAL A LOCALZA��O DA ILHA DE NATAL IMPLEMENTADO?'
				AND pr.[Form Visibilidade Name] = 'Ilha de Natal - LOCALZA��O'
				AND pr.formItemOptionText = 'CANTINHO CHURRASCO'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
			,
			0 * (
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement = 'QUAL A LOCALZA��O DA ILHA DE NATAL IMPLEMENTADO?'
				AND pr.[Form Visibilidade Name] = 'Ilha de Natal - LOCALZA��O'
				AND pr.formItemOptionText <> 'CANTINHO CHURRASCO'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
	) AS [Ilha de Natal - Cantinho Churrasco]
	, COALESCE(
			(
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement = 'QUAL A LOCALZA��O DA ILHA DE NATAL IMPLEMENTADO?'
				AND pr.[Form Visibilidade Name] = 'Ilha de Natal - LOCALZA��O'
				AND pr.formItemOptionText = 'OUTROS'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
			,
			0 * (
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement = 'QUAL A LOCALZA��O DA ILHA DE NATAL IMPLEMENTADO?'
				AND pr.[Form Visibilidade Name] = 'Ilha de Natal - LOCALZA��O'
				AND pr.formItemOptionText <> 'OUTROS'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
	) AS [Ilha de Natal - Outros]
	, COALESCE(
			(
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement = 'O PDV TEM �RVORE DE NATAL PREMIUM IMPLEMENTADA?'
				AND pr.[Form Visibilidade Name] = '�rvore de Natal Premium'
				AND pr.formItemOptionText = 'SIM'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
			,
			0 * (
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement = 'O PDV TEM �RVORE DE NATAL PREMIUM IMPLEMENTADA?'
				AND pr.[Form Visibilidade Name] = '�rvore de Natal Premium'
				AND pr.formItemOptionText <> 'SIM'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
	) AS [�rvore de Natal Premium]
	, COALESCE(
			(
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement = 'QUAIS MARCAS EST�O IMPLEMENTADAS NA ARVORE DE NATAL PREMIUM?'
				AND pr.[Form Visibilidade Name] = '�rvore de Natal Premium - Marcas'
				AND pr.formItemOptionText = 'JW BLACK LABEL 1L'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
			,
			0 * (
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement = 'QUAIS MARCAS EST�O IMPLEMENTADAS NA ARVORE DE NATAL PREMIUM?'
				AND pr.[Form Visibilidade Name] = '�rvore de Natal Premium - Marcas'
				AND pr.formItemOptionText <> 'JW BLACK LABEL 1L'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
	) AS [�rvore de Natal Premium - JW BLACK LABEL 1L]
	, COALESCE(
			(
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement = 'QUAIS MARCAS EST�O IMPLEMENTADAS NA ARVORE DE NATAL PREMIUM?'
				AND pr.[Form Visibilidade Name] = '�rvore de Natal Premium - Marcas'
				AND pr.formItemOptionText = 'JW BLACK LABEL 750ml'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
			,
			0 * (
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement = 'QUAIS MARCAS EST�O IMPLEMENTADAS NA ARVORE DE NATAL PREMIUM?'
				AND pr.[Form Visibilidade Name] = '�rvore de Natal Premium - Marcas'
				AND pr.formItemOptionText <> 'JW BLACK LABEL 750ml'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
	) AS [�rvore de Natal Premium - JW BLACK LABEL 750ml]
	, COALESCE(
			(
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement = 'QUAIS MARCAS EST�O IMPLEMENTADAS NA ARVORE DE NATAL PREMIUM?'
				AND pr.[Form Visibilidade Name] = '�rvore de Natal Premium - Marcas'
				AND pr.formItemOptionText = 'JW GOLD LABEL 750ML'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
			, 
			0 * (
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement = 'QUAIS MARCAS EST�O IMPLEMENTADAS NA ARVORE DE NATAL PREMIUM?'
				AND pr.[Form Visibilidade Name] = '�rvore de Natal Premium - Marcas'
				AND pr.formItemOptionText <> 'JW GOLD LABEL 750ML'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
	) AS [�rvore de Natal Premium - JW GOLD LABEL 750ML]
	, COALESCE(
			(
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement = 'QUAL A LOCALZA��O DA �RVORE DE NATAL PREMIUM IMPLEMENTADA?'
				AND pr.[Form Visibilidade Name] = '�rvore de Natal Premium - LOCALZA��O'
				AND pr.formItemOptionText = 'CORREDOR DIRETOR'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
			,
			0 * (
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement = 'QUAL A LOCALZA��O DA �RVORE DE NATAL PREMIUM IMPLEMENTADA?'
				AND pr.[Form Visibilidade Name] = '�rvore de Natal Premium - LOCALZA��O'
				AND pr.formItemOptionText <> 'CORREDOR DIRETOR'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
	) AS [�rvore de Natal Premium - Corredor Diretor]
	, COALESCE(
			(
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement = 'QUAL A LOCALZA��O DA �RVORE DE NATAL PREMIUM IMPLEMENTADA?'
				AND pr.[Form Visibilidade Name] = '�rvore de Natal Premium - LOCALZA��O'
				AND pr.formItemOptionText = 'CORREDOR CATEGORIA'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
			,
			0 * (
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement = 'QUAL A LOCALZA��O DA �RVORE DE NATAL PREMIUM IMPLEMENTADA?'
				AND pr.[Form Visibilidade Name] = '�rvore de Natal Premium - LOCALZA��O'
				AND pr.formItemOptionText <> 'CORREDOR CATEGORIA'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
	) AS [�rvore de Natal Premium - Corredor Categoria]
	, COALESCE(
			(
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement = 'QUAL A LOCALZA��O DA �RVORE DE NATAL PREMIUM IMPLEMENTADA?'
				AND pr.[Form Visibilidade Name] = '�rvore de Natal Premium - LOCALZA��O'
				AND pr.formItemOptionText = 'CORREDOR CERVEJA'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
			,
			0 * (
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement = 'QUAL A LOCALZA��O DA �RVORE DE NATAL PREMIUM IMPLEMENTADA?'
				AND pr.[Form Visibilidade Name] = '�rvore de Natal Premium - LOCALZA��O'
				AND pr.formItemOptionText <> 'CORREDOR CERVEJA'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
	) AS [�rvore de Natal Premium - Corredor Cerveja]
	, COALESCE(
			(
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement = 'QUAL A LOCALZA��O DA �RVORE DE NATAL PREMIUM IMPLEMENTADA?'
				AND pr.[Form Visibilidade Name] = '�rvore de Natal Premium - LOCALZA��O'
				AND pr.formItemOptionText = 'CANTINHO CHURRASCO'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
			,
			0 * (
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement = 'QUAL A LOCALZA��O DA �RVORE DE NATAL PREMIUM IMPLEMENTADA?'
				AND pr.[Form Visibilidade Name] = '�rvore de Natal Premium - LOCALZA��O'
				AND pr.formItemOptionText <> 'CANTINHO CHURRASCO'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
	) AS [�rvore de Natal Premium - Cantinho Churrasco]
	, COALESCE(
			(
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement = 'QUAL A LOCALZA��O DA �RVORE DE NATAL PREMIUM IMPLEMENTADA?'
				AND pr.[Form Visibilidade Name] = '�rvore de Natal Premium - LOCALZA��O'
				AND pr.formItemOptionText = 'OUTROS'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
			,
			0 * (
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement = 'QUAL A LOCALZA��O DA �RVORE DE NATAL PREMIUM IMPLEMENTADA?'
				AND pr.[Form Visibilidade Name] = '�rvore de Natal Premium - LOCALZA��O'
				AND pr.formItemOptionText <> 'OUTROS'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
	) AS [�rvore de Natal Premium - Outros]
	, COALESCE(
			(
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement = 'O PDV TEM ALGUM OUTRO TIPO DE PONTO EXTRA COM EXECU��O DE JW IMPLEMENTADO?'
				AND pr.[Form Visibilidade Name] = 'Outro Ponto Extra'
				AND pr.formItemOptionText = 'SIM'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
			,
			0 * (
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement = 'O PDV TEM ALGUM OUTRO TIPO DE PONTO EXTRA COM EXECU��O DE JW IMPLEMENTADO?'
				AND pr.[Form Visibilidade Name] = 'Outro Ponto Extra'
				AND pr.formItemOptionText <> 'SIM'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
	) AS [Outro Ponto Extra]
	, COALESCE(
			(
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement = 'QUAL A LOCALZA��O DESSE OUTRO TIPO DE PONTO EXTRA COM EXECU��O JW?'
				AND pr.[Form Visibilidade Name] = 'Outro Ponto Extra - LOCALZA��O'
				AND pr.formItemOptionText = 'CORREDOR DIRETOR'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
			,
			0 * (
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement = 'QUAL A LOCALZA��O DESSE OUTRO TIPO DE PONTO EXTRA COM EXECU��O JW?'
				AND pr.[Form Visibilidade Name] = 'Outro Ponto Extra - LOCALZA��O'
				AND pr.formItemOptionText <> 'CORREDOR DIRETOR'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
	) AS [Outro Ponto Extra - Corredor Diretor]
	, COALESCE(
			(
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement = 'QUAL A LOCALZA��O DESSE OUTRO TIPO DE PONTO EXTRA COM EXECU��O JW?'
				AND pr.[Form Visibilidade Name] = 'Outro Ponto Extra - LOCALZA��O'
				AND pr.formItemOptionText = 'CORREDOR CATEGORIA'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
			,
			0 * (
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement = 'QUAL A LOCALZA��O DESSE OUTRO TIPO DE PONTO EXTRA COM EXECU��O JW?'
				AND pr.[Form Visibilidade Name] = 'Outro Ponto Extra - LOCALZA��O'
				AND pr.formItemOptionText <> 'CORREDOR CATEGORIA'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
	) AS [Outro Ponto Extra - Corredor Categoria]
	, COALESCE(
			(
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement = 'QUAL A LOCALZA��O DESSE OUTRO TIPO DE PONTO EXTRA COM EXECU��O JW?'
				AND pr.[Form Visibilidade Name] = 'Outro Ponto Extra - LOCALZA��O'
				AND pr.formItemOptionText = 'CORREDOR CERVEJA'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
			,
			0 * (
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement = 'QUAL A LOCALZA��O DESSE OUTRO TIPO DE PONTO EXTRA COM EXECU��O JW?'
				AND pr.[Form Visibilidade Name] = 'Outro Ponto Extra - LOCALZA��O'
				AND pr.formItemOptionText <> 'CORREDOR CERVEJA'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
	) AS [Outro Ponto Extra - Corredor Cerveja]
	, COALESCE(
			(
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement = 'QUAL A LOCALZA��O DESSE OUTRO TIPO DE PONTO EXTRA COM EXECU��O JW?'
				AND pr.[Form Visibilidade Name] = 'Outro Ponto Extra - LOCALZA��O'
				AND pr.formItemOptionText = 'CANTINHO CHURRASCO'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
			,
			0 * (
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement = 'QUAL A LOCALZA��O DESSE OUTRO TIPO DE PONTO EXTRA COM EXECU��O JW?'
				AND pr.[Form Visibilidade Name] = 'Outro Ponto Extra - LOCALZA��O'
				AND pr.formItemOptionText <> 'CANTINHO CHURRASCO'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
	) AS [Outro Ponto Extra - Cantinho Churrasco]
	, COALESCE(
			(
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement = 'QUAL A LOCALZA��O DESSE OUTRO TIPO DE PONTO EXTRA COM EXECU��O JW?'
				AND pr.[Form Visibilidade Name] = 'Outro Ponto Extra - LOCALZA��O'
				AND pr.formItemOptionText = 'OUTROS'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
			,
			0 * (
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement = 'QUAL A LOCALZA��O DESSE OUTRO TIPO DE PONTO EXTRA COM EXECU��O JW?'
				AND pr.[Form Visibilidade Name] = 'Outro Ponto Extra - LOCALZA��O'
				AND pr.formItemOptionText <> 'OUTROS'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
	) AS [Outro Ponto Extra - Outros]
	, COALESCE(
			(
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement = 'QUAIS MARCAS EST�O IMPLEMENTADAS NESSE OUTRO TIPO DE PONTO EXTRA COM EXECU��O JW?'
				AND pr.[Form Visibilidade Name] = 'Outro Ponto Extra - Marcas'
				AND pr.formItemOptionText = 'JW RED LABEL 1L'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
			,
			0 * (
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement = 'QUAIS MARCAS EST�O IMPLEMENTADAS NESSE OUTRO TIPO DE PONTO EXTRA COM EXECU��O JW?'
				AND pr.[Form Visibilidade Name] = 'Outro Ponto Extra - Marcas'
				AND pr.formItemOptionText <> 'JW RED LABEL 1L'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
	) AS [Outro Ponto Extra - JW RED LABEL 1L]
	, COALESCE(
			(
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement = 'QUAIS MARCAS EST�O IMPLEMENTADAS NESSE OUTRO TIPO DE PONTO EXTRA COM EXECU��O JW?'
				AND pr.[Form Visibilidade Name] = 'Outro Ponto Extra - Marcas'
				AND pr.formItemOptionText = 'JW RED LABEL 750ML'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
			,
			0 * (
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement = 'QUAIS MARCAS EST�O IMPLEMENTADAS NESSE OUTRO TIPO DE PONTO EXTRA COM EXECU��O JW?'
				AND pr.[Form Visibilidade Name] = 'Outro Ponto Extra - Marcas'
				AND pr.formItemOptionText <> 'JW RED LABEL 750ML'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
	) AS [Outro Ponto Extra - JW RED LABEL 750ML]
	, COALESCE(
			(
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement = 'QUAIS MARCAS EST�O IMPLEMENTADAS NESSE OUTRO TIPO DE PONTO EXTRA COM EXECU��O JW?'
				AND pr.[Form Visibilidade Name] = 'Outro Ponto Extra - Marcas'
				AND pr.formItemOptionText = 'JW BLACK LABEL 1L'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
			,
			0 * (
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement = 'QUAIS MARCAS EST�O IMPLEMENTADAS NESSE OUTRO TIPO DE PONTO EXTRA COM EXECU��O JW?'
				AND pr.[Form Visibilidade Name] = 'Outro Ponto Extra - Marcas'
				AND pr.formItemOptionText <> 'JW BLACK LABEL 1L'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
	) AS [Outro Ponto Extra - JW BLACK LABEL 1L]
	, COALESCE(
			(
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement = 'QUAIS MARCAS EST�O IMPLEMENTADAS NESSE OUTRO TIPO DE PONTO EXTRA COM EXECU��O JW?'
				AND pr.[Form Visibilidade Name] = 'Outro Ponto Extra - Marcas'
				AND pr.formItemOptionText = 'JW BLACK LABEL 750ML'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
			,
			0 * (
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement = 'QUAIS MARCAS EST�O IMPLEMENTADAS NESSE OUTRO TIPO DE PONTO EXTRA COM EXECU��O JW?'
				AND pr.[Form Visibilidade Name] = 'Outro Ponto Extra - Marcas'
				AND pr.formItemOptionText <> 'JW BLACK LABEL 750ML'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
	) AS [Outro Ponto Extra - JW BLACK LABEL 750ML]
	, COALESCE(
			(
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement = 'QUAIS MARCAS EST�O IMPLEMENTADAS NESSE OUTRO TIPO DE PONTO EXTRA COM EXECU��O JW?'
				AND pr.[Form Visibilidade Name] = 'Outro Ponto Extra - Marcas'
				AND pr.formItemOptionText = 'JW RED RYE'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
			,
			0 * (
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement = 'QUAIS MARCAS EST�O IMPLEMENTADAS NESSE OUTRO TIPO DE PONTO EXTRA COM EXECU��O JW?'
				AND pr.[Form Visibilidade Name] = 'Outro Ponto Extra - Marcas'
				AND pr.formItemOptionText <> 'JW RED RYE'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
	) AS [Outro Ponto Extra - JW RED RYE]
	, COALESCE(
			(
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement = 'QUAIS MARCAS EST�O IMPLEMENTADAS NESSE OUTRO TIPO DE PONTO EXTRA COM EXECU��O JW?'
				AND pr.[Form Visibilidade Name] = 'Outro Ponto Extra - Marcas'
				AND pr.formItemOptionText = 'JW GOLD 750ML'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
			,
			0 * (
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement = 'QUAIS MARCAS EST�O IMPLEMENTADAS NESSE OUTRO TIPO DE PONTO EXTRA COM EXECU��O JW?'
				AND pr.[Form Visibilidade Name] = 'Outro Ponto Extra - Marcas'
				AND pr.formItemOptionText <> 'JW GOLD 750ML'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
	) AS [Outro Ponto Extra - JW GOLD 750ML]
	, COALESCE(
			(
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement = 'QUAIS MARCAS EST�O IMPLEMENTADAS NESSE OUTRO TIPO DE PONTO EXTRA COM EXECU��O JW?'
				AND pr.[Form Visibilidade Name] = 'Outro Ponto Extra - Marcas'
				AND pr.formItemOptionText = 'OUTROS'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
			,
			0 * (
				SELECT MAX(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.isCompleted = 1
				--AND pr.formItemStatement = 'QUAIS MARCAS EST�O IMPLEMENTADAS NESSE OUTRO TIPO DE PONTO EXTRA COM EXECU��O JW?'
				AND pr.[Form Visibilidade Name] = 'Outro Ponto Extra - Marcas'
				AND pr.formItemOptionText <> 'OUTROS'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			)
	) AS [Outro Ponto Extra - OUTROS]
FROM t AS l


