
DECLARE @startDate Datetime = '2017-12-17', @endDate Datetime = '2017-12-23'
DECLARE @formTypeCode Varchar(10) = 'EX' --EX

;WITH t 
AS
(
	select lc.locationName AS PDV
	, lc.locationReference AS CNPJ
	, lc.locationId
	, Products.[Form Product List Name] AS [Codigo Produto]
	, Products.[Form Product List Category] AS [Categoria Producto]
	, Products.[Form Product List Standard Name] AS [Descripcion Producto]
	, DATENAME(MONTH, @endDate)+'/'+CAST(YEAR(@endDate) AS VARCHAR(4)) AS M�s
		, CAST(DAY(@startDate) AS VARCHAR(2))+'/'+CAST(MONTH(@startDate) AS VARCHAR(2))+' a '+CAST(DAY(@endDate) AS VARCHAR(2))+'/'+CAST(MONTH(@endDate) AS VARCHAR(2)) AS Semana
	, @formTypeCode AS [Form Type Code]
	from [DW].LocationsOTDiretoDiciembre as l
	inner join [DW].Location as lc on l.CNPJ = lc.locationReference
	inner join
	(
			select pl.*
			from [DW].ProductosOTDiretoNew as p
			inner join 
			(
					select av.attributeValue as [Form Product List Name], 
					(
						select av2.attributeValue
						from [System].tbl_Attributes_Values as av2
						where av2.categoryItemId = av.categoryItemId and av2.attributeId = 94
					) as [Form Product List Category], 
					(
						select av2.attributeValue
						from [System].tbl_Attributes_Values as av2
						where av2.categoryItemId = av.categoryItemId and av2.attributeId = 99
					) as [Form Product List Standard Name]
					from [System].tbl_Category_Items as ci
					inner join [System].tbl_Attributes_Values as av on ci.categoryItemId = av.categoryItemId
					where ci.categoryId = 51 and av.attributeId = 91

			) as pl on p.Codigo = pl. [Form Product List Name]

	) as Products on 1 = 1
)

SELECT l.*
	/*,(
		SELECT MAX(pr.auditedBy)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.[Form Product List Name] = l.[Codigo Produto]
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					AND pr.planResultDetailIsDeleted = 0
					AND pr.[Form Template Form Type Code] = @formTypeCode
	) AS auditedBy*/
	, COALESCE(
				CAST(
				(
					SELECT MAX(pr.measureAnswer)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.[Form Product List Name] = l.[Codigo Produto]
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.[Form Sales Drivers Name] = 'DISTRIBUI��O'
					AND pr.isCompleted = 1
					AND pr.formItemOptionText = 'NA CASA'
					AND pr.planResultDetailIsDeleted = 0
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				) AS VARCHAR(50))
				, CAST(
				0 * (
					SELECT MAX(pr.measureAnswer)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.[Form Product List Name] = l.[Codigo Produto]
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.[Form Sales Drivers Name] = 'DISTRIBUI��O'
					AND pr.isCompleted = 1
					AND pr.formItemOptionText <> 'NA CASA'
					AND pr.planResultDetailIsDeleted = 0
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				) AS VARCHAR(50))
				, 'NA'
	) AS [Presen�a na Casa]
	, COALESCE(
				CAST(
				(
					SELECT MAX(pr.measureAnswer)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.[Form Product List Name] = l.[Codigo Produto]
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.[Form Sales Drivers Name] = 'DISTRIBUI��O'
					AND pr.isCompleted = 1
					AND pr.formItemOptionText = 'NO CARD�PIO'
					AND pr.planResultDetailIsDeleted = 0
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				) AS VARCHAR(50))
				, CAST(
				0 * (
					SELECT MAX(pr.measureAnswer)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.[Form Product List Name] = l.[Codigo Produto]
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.[Form Sales Drivers Name] = 'DISTRIBUI��O'
					AND pr.isCompleted = 1
					AND pr.formItemOptionText <> 'NO CARD�PIO'
					AND pr.planResultDetailIsDeleted = 0
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				) AS VARCHAR(50))
				, 'NA'
	) AS [Presen�a no Card�pio]
	, COALESCE(
		CAST(
		(
			SELECT AVG(pr.measureAnswer)
			FROM [DW].vw_PRDFIO AS pr
			WHERE pr.locationId = l.locationId 
			AND pr.[Form Product List Name] = l.[Codigo Produto]
			AND pr.dateAudited BETWEEN @startDate AND @endDate
			AND pr.[Form Sales Drivers Name] = 'PRE�O'
			AND pr.isCompleted = 1
			AND pr.measureAnswer > 0
			AND pr.[Form Pre�o Name] = 'DOSE'
			AND pr.planResultDetailIsDeleted = 0
			AND pr.[Form Template Form Type Code] = @formTypeCode
			AND pr.auditedBy NOT LIKE '%test%'
		) AS VARCHAR(50))
		, 'NA'
	) AS [Pre�o Dose]
	, COALESCE
		(
			CAST(
			(
				SELECT AVG(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId 
				AND pr.[Form Product List Name] = l.[Codigo Produto]
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.[Form Sales Drivers Name] = 'PRE�O'
				AND pr.isCompleted = 1
				AND pr.measureAnswer > 0
				AND pr.[Form Pre�o Name] = 'GARRAFA'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			) AS VARCHAR(50))
			, 'NA'
		) AS [Pre�o Garrafa]
	, COALESCE(
		CAST(
			(
				SELECT AVG(pr.measureAnswer)
				FROM [DW].vw_PRDFIO AS pr
				WHERE pr.locationId = l.locationId 
				AND pr.[Form Product List Name] = l.[Codigo Produto]
				AND pr.dateAudited BETWEEN @startDate AND @endDate
				AND pr.[Form Sales Drivers Name] = 'PRE�O'
				AND pr.isCompleted = 1
				AND pr.measureAnswer > 0
				AND pr.[Form Pre�o Name] = 'DRINK'
				AND pr.planResultDetailIsDeleted = 0
				AND pr.[Form Template Form Type Code] = @formTypeCode
				AND pr.auditedBy NOT LIKE '%test%'
			) AS VARCHAR(50))
			, 'NA'
		) AS [Pre�o Drink]
	, COALESCE(
				CAST(
				(
					SELECT MAX(pr.measureAnswer)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.[Form Qualidade Name] = 'Tanqueray & Tonic em Copa Glass'
					AND pr.isCompleted = 1
					AND pr.planResultDetailIsDeleted = 0
					AND pr.formItemOptionText = 'SIM'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				) AS VARCHAR(50))
				, CAST(
				0 * (
					SELECT MAX(pr.measureAnswer)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.[Form Qualidade Name] = 'Tanqueray & Tonic em Copa Glass'
					AND pr.isCompleted = 1
					AND pr.planResultDetailIsDeleted = 0
					AND pr.formItemOptionText <> 'SIM'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				) AS VARCHAR(50))
				, 'NA'
	) AS [Tanqueray & Tonic em Copa Glass]
	, COALESCE(
				CAST(
				(
					SELECT MAX(pr.measureAnswer)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.[Form Qualidade Name] = 'Tanqueray & Tonic Perfect Serve'
					AND pr.isCompleted = 1
					AND pr.planResultDetailIsDeleted = 0
					AND pr.formItemOptionText = 'SIM'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				) AS VARCHAR(50))
				, CAST(
				0 * (
					SELECT MAX(pr.measureAnswer)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.[Form Qualidade Name] = 'Tanqueray & Tonic Perfect Serve'
					AND pr.isCompleted = 1
					AND pr.planResultDetailIsDeleted = 0
					AND pr.formItemOptionText <> 'SIM'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				) AS VARCHAR(50))
				, 'NA'
	) AS [Tanqueray & Tonic Perfect Serve]
	, COALESCE(
				CAST(
				(
					SELECT MAX(pr.measureAnswer)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					AND pr.planResultDetailIsDeleted = 0
					AND pr.[Form Visibilidade Name] = 'Tailor Made'
					AND pr.formItemOptionText = 'SIM'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				) AS VARCHAR(50))
				, CAST(
				0 * (
					SELECT MAX(pr.measureAnswer)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					AND pr.planResultDetailIsDeleted = 0
					AND pr.[Form Visibilidade Name] = 'Tailor Made'
					AND pr.formItemOptionText <> 'SIM'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				) AS VARCHAR(50))
				, 'NA'
	) AS [Tailor Made]
	, COALESCE(
				CAST(
				(
					SELECT MAX(pr.measureAnswer)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					AND pr.measureAnswer > 0
					AND pr.planResultDetailIsDeleted = 0
					AND pr.[Form Visibilidade Name] = 'Back Bar + 50% do espa�o'
					AND pr.formItemOptionText = 'SIM'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				) AS VARCHAR(50))
				, CAST(
				0 * (
					SELECT MAX(pr.measureAnswer)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					AND pr.measureAnswer > 0
					AND pr.planResultDetailIsDeleted = 0
					AND pr.[Form Visibilidade Name] = 'Back Bar + 50% do espa�o'
					AND pr.formItemOptionText <> 'SIM'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				) AS VARCHAR(50))
				, 'NA'
	) AS [Back Bar +50% do espa�o]
	,
	(
		COALESCE(
			CAST(
				(
					SELECT MAX(pr.measureAnswer)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					AND pr.planResultDetailIsDeleted = 0
					--AND pr.formItemStatement = 'A CASA POSSUI A��O "PAINEL DE LED" IMPLEMENTADO?'
					AND pr.[Form Promo��o Name] = 'Painel de LED'
					AND pr.formItemOptionText = 'SIM'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				)
			 AS VARCHAR(50))
			, CAST(
				0 * (
					SELECT MAX(pr.measureAnswer)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					AND pr.planResultDetailIsDeleted = 0
					--AND pr.formItemStatement = 'A CASA POSSUI A��O "PAINEL DE LED" IMPLEMENTADO?'
					AND pr.[Form Promo��o Name] = 'Painel de LED'
					AND pr.formItemOptionText <> 'SIM'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				)
			 AS VARCHAR(50))
			, 'NA'
		)
	) AS [Painel de LED]
	,
	(
		COALESCE(
			CAST(
				(
					SELECT MAX(pr.measureAnswer)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					AND pr.planResultDetailIsDeleted = 0
					--AND pr.formItemStatement = 'SELECIONE AS MARCAS IMPLEMENTADAS NESSA A��O'
					AND pr.[Form Promo��o Name] = 'Painel de LED - Marcas'
					AND pr.formItemOptionText = 'JOHNNIE WALKER'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				)
			 AS VARCHAR(50))
			, CAST(
				0 * (
					SELECT MAX(pr.measureAnswer)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					AND pr.planResultDetailIsDeleted = 0
					--AND pr.formItemStatement = 'SELECIONE AS MARCAS IMPLEMENTADAS NESSA A��O'
					AND pr.[Form Promo��o Name] = 'Painel de LED - Marcas'
					AND pr.formItemOptionText != 'JOHNNIE WALKER'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				)
			 AS VARCHAR(50))
			, 'NA'
		)
	) AS [Painel de LED - JOHNNIE WALKER]
	,
	(
		COALESCE(
			CAST(
				(
					SELECT MAX(pr.measureAnswer)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					AND pr.planResultDetailIsDeleted = 0
					--AND pr.formItemStatement = 'SELECIONE AS MARCAS IMPLEMENTADAS NESSA A��O'
					AND pr.[Form Promo��o Name] = 'Painel de LED - Marcas'
					AND pr.formItemOptionText = 'SMIRNOFF'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				)
			 AS VARCHAR(50))
			, CAST(
				0 * (
					SELECT MAX(pr.measureAnswer)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					AND pr.planResultDetailIsDeleted = 0
					--AND pr.formItemStatement = 'SELECIONE AS MARCAS IMPLEMENTADAS NESSA A��O'
					AND pr.[Form Promo��o Name] = 'Painel de LED - Marcas'
					AND pr.formItemOptionText != 'SMIRNOFF'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				)
			 AS VARCHAR(50))
			, 'NA'
		)
	) AS [Painel de LED - SMIRNOFF]
	,
	(
		COALESCE(
			CAST(
				(
					SELECT MAX(pr.measureAnswer)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					AND pr.planResultDetailIsDeleted = 0
					--AND pr.formItemStatement = 'SELECIONE AS MARCAS IMPLEMENTADAS NESSA A��O'
					AND pr.[Form Promo��o Name] = 'Painel de LED - Marcas'
					AND pr.formItemOptionText = 'CIROC'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				)
			 AS VARCHAR(50))
			, CAST(
				0 * (
					SELECT MAX(pr.measureAnswer)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					AND pr.planResultDetailIsDeleted = 0
					--AND pr.formItemStatement = 'SELECIONE AS MARCAS IMPLEMENTADAS NESSA A��O'
					AND pr.[Form Promo��o Name] = 'Painel de LED - Marcas'
					AND pr.formItemOptionText != 'CIROC'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				)
			 AS VARCHAR(50))
			, 'NA'
		)
	) AS [Painel de LED - CIROC]
	,
	(
		COALESCE(
			CAST(
				(
					SELECT MAX(pr.measureAnswer)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					AND pr.planResultDetailIsDeleted = 0
					--AND pr.formItemStatement = 'A CASA POSSUI O MENU DE COMBOS E DRINKS SIMPLE MIX IMPLEMENTADO?'
					AND pr.[Form Promo��o Name] = 'Menu de Combos Drink'
					AND pr.formItemOptionText = 'SIM'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				)
			 AS VARCHAR(50))
			, CAST(
				0 * (
					SELECT MAX(pr.measureAnswer)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					AND pr.planResultDetailIsDeleted = 0
					--AND pr.formItemStatement = 'A CASA POSSUI O MENU DE COMBOS E DRINKS SIMPLE MIX IMPLEMENTADO?'
					AND pr.[Form Promo��o Name] = 'Menu de Combos Drink'
					AND pr.formItemOptionText <> 'SIM'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				)
			 AS VARCHAR(50))
			, 'NA'
		)
	) AS [Menu de Combos e Drinks Simple Mix]
	,
	(
		COALESCE(
			CAST(
				(
					SELECT MAX(pr.measureAnswer)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					AND pr.planResultDetailIsDeleted = 0
					--AND pr.formItemStatement = 'A CASA POSSUI COPOS OU TA�AS DA DIAGEO IMPLEMENTADA?'
					AND pr.[Form Promo��o Name] = 'Copos ou Ta�as Diageo'
					AND pr.formItemOptionText = 'SIM'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				)
			 AS VARCHAR(50))
			, CAST(
				0 * (
					SELECT MAX(pr.measureAnswer)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					AND pr.planResultDetailIsDeleted = 0
					--AND pr.formItemStatement = 'A CASA POSSUI COPOS OU TA�AS DA DIAGEO IMPLEMENTADA?'
					AND pr.[Form Promo��o Name] = 'Copos ou Ta�as Diageo'
					AND pr.formItemOptionText <> 'SIM'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				)
			 AS VARCHAR(50))
			, 'NA'
		)
	) AS [Copos ou Ta�as Diageo]
	,
	(
		COALESCE(
			CAST(
				(
					SELECT MAX(pr.measureAnswer)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					AND pr.planResultDetailIsDeleted = 0
					--AND pr.formItemStatement = 'SELECIONE AS MARCAS IMPLEMENTADAS'
					AND pr.[Form Promo��o Name] = 'Copos ou Ta�as Diageo - Marcas'
					AND pr.formItemOptionText = 'JOHNNIE WALKER'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				)
			 AS VARCHAR(50))
			, CAST(
				0 * (
					SELECT MAX(pr.measureAnswer)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					AND pr.planResultDetailIsDeleted = 0
					--AND pr.formItemStatement = 'SELECIONE AS MARCAS IMPLEMENTADAS'
					AND pr.[Form Promo��o Name] = 'Copos ou Ta�as Diageo - Marcas'
					AND pr.formItemOptionText != 'JOHNNIE WALKER'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				)
			 AS VARCHAR(50))
			, 'NA'
		)
	) AS [Copos ou Ta�as Diageo - JOHNNIE WALKER]
	,
	(
		COALESCE(
			CAST(
				(
					SELECT MAX(pr.measureAnswer)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					AND pr.planResultDetailIsDeleted = 0
					--AND pr.formItemStatement = 'SELECIONE AS MARCAS IMPLEMENTADAS'
					AND pr.[Form Promo��o Name] = 'Copos ou Ta�as Diageo - Marcas'
					AND pr.formItemOptionText = 'CIROC'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				)
			 AS VARCHAR(50))
			, CAST(
				0 * (
					SELECT MAX(pr.measureAnswer)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					AND pr.planResultDetailIsDeleted = 0
					--AND pr.formItemStatement = 'SELECIONE AS MARCAS IMPLEMENTADAS'
					AND pr.[Form Promo��o Name] = 'Copos ou Ta�as Diageo - Marcas'
					AND pr.formItemOptionText != 'CIROC'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				)
			 AS VARCHAR(50))
			, 'NA'
		)
	) AS [Copos ou Ta�as Diageo - CIROC]
	,
	(
		COALESCE(
			CAST(
				(
					SELECT MAX(pr.measureAnswer)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					AND pr.planResultDetailIsDeleted = 0
					--AND pr.formItemStatement = 'SELECIONE AS MARCAS IMPLEMENTADAS'
					AND pr.[Form Promo��o Name] = 'Copos ou Ta�as Diageo - Marcas'
					AND pr.formItemOptionText = 'TANQUERAY'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				)
			 AS VARCHAR(50))
			, CAST(
				0 * (
					SELECT MAX(pr.measureAnswer)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					AND pr.planResultDetailIsDeleted = 0
					--AND pr.formItemStatement = 'SELECIONE AS MARCAS IMPLEMENTADAS'
					AND pr.[Form Promo��o Name] = 'Copos ou Ta�as Diageo - Marcas'
					AND pr.formItemOptionText != 'TANQUERAY'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				)
			 AS VARCHAR(50))
			, 'NA'
		)
	) AS [Copos ou Ta�as Diageo - TANQUERAY]
	,
	(
		COALESCE(
			CAST(
				(
					SELECT MAX(pr.measureAnswer)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					AND pr.planResultDetailIsDeleted = 0
					--AND pr.formItemStatement = 'CASA POSSUI A A��O "CHAMPANHEIRA COMBO CIROC" OU "BALDE COMBO CIROC/SMIRNOFF" IMPLEMENTADA ?'
					AND pr.[Form Promo��o Name] = 'Champanheira Combo'
					AND pr.formItemOptionText = 'SIM'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				)
			 AS VARCHAR(50))
			, CAST(
				0 * (
					SELECT MAX(pr.measureAnswer)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					AND pr.planResultDetailIsDeleted = 0
					--AND pr.formItemStatement = 'CASA POSSUI A A��O "CHAMPANHEIRA COMBO CIROC" OU "BALDE COMBO CIROC/SMIRNOFF" IMPLEMENTADA ?'
					AND pr.[Form Promo��o Name] = 'Champanheira Combo'
					AND pr.formItemOptionText <> 'SIM'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				)
			 AS VARCHAR(50))
			, 'NA'
		)
	) AS [Champanheira Combo Ciroc ou Balde Combo Ciroc/Smirnoff]
	,
	(
		COALESCE(
			CAST(
				(
					SELECT MAX(pr.measureAnswer)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					AND pr.planResultDetailIsDeleted = 0
					--AND pr.formItemStatement = 'A CASA POSSUI A A��O "DRINK FESTIVAL" IMPLEMENTADA?'
					AND pr.[Form Promo��o Name] = 'Drink Festival'
					AND pr.formItemOptionText = 'SIM'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				)
			 AS VARCHAR(50))
			, CAST(
				0 * (
					SELECT MAX(pr.measureAnswer)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					AND pr.planResultDetailIsDeleted = 0
					--AND pr.formItemStatement = 'A CASA POSSUI A A��O "DRINK FESTIVAL" IMPLEMENTADA?'
					AND pr.[Form Promo��o Name] = 'Drink Festival'
					AND pr.formItemOptionText <> 'SIM'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				)
			 AS VARCHAR(50))
			, 'NA'
		)
	) AS [Drink Festival]
	,
	(
		COALESCE(
			CAST(
				(
					SELECT MAX(pr.measureAnswer)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					AND pr.planResultDetailIsDeleted = 0
					--AND pr.formItemStatement = 'A CASA PARTICIPA DO PROGRAMA DBA?'
					AND pr.[Form Promo��o Name] = 'Programa DBA'
					AND pr.formItemOptionText = 'SIM'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				)
			 AS VARCHAR(50))
			, CAST(
				0 * (
					SELECT MAX(pr.measureAnswer)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					AND pr.planResultDetailIsDeleted = 0
					--AND pr.formItemStatement = 'A CASA PARTICIPA DO PROGRAMA DBA?'
					AND pr.[Form Promo��o Name] = 'Programa DBA'
					AND pr.formItemOptionText <> 'SIM'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				)
			 AS VARCHAR(50))
			, 'NA'
		)
	) AS [Programa DBA]
	,
	(
		COALESCE(
			CAST(
				(
					SELECT MAX(pr.measureAnswer)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					AND pr.planResultDetailIsDeleted = 0
					--AND pr.formItemStatement = 'A CASA POSSUI CLUBE DO WISKY IMPLEMENTADO?'
					AND pr.[Form Promo��o Name] = 'Clube do Whisky'
					AND pr.formItemOptionText = 'SIM'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				)
			 AS VARCHAR(50))
			, CAST(
				0 * (
					SELECT MAX(pr.measureAnswer)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					AND pr.planResultDetailIsDeleted = 0
					--AND pr.formItemStatement = 'A CASA POSSUI CLUBE DO WISKY IMPLEMENTADO?'
					AND pr.[Form Promo��o Name] = 'Clube do Whisky'
					AND pr.formItemOptionText <> 'SIM'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				)
			 AS VARCHAR(50))
			, 'NA'
		)
	) AS [Clube do Whisky]
	,
	(
		COALESCE(
			CAST(
				(
					SELECT MAX(pr.measureAnswer)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					AND pr.planResultDetailIsDeleted = 0
					--AND pr.formItemStatement = 'SELECIONE AS MARCAS QUE EST�O PRESENTES NO CLUBE DO WISKY'
					AND pr.[Form Promo��o Name] = 'Clube do Whisky - Marcas'
					AND pr.formItemOptionText = 'JW RED LABEL'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				)
			 AS VARCHAR(50))
			, CAST(
				0 * (
					SELECT MAX(pr.measureAnswer)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					AND pr.planResultDetailIsDeleted = 0
					--AND pr.formItemStatement = 'SELECIONE AS MARCAS QUE EST�O PRESENTES NO CLUBE DO WISKY'
					AND pr.[Form Promo��o Name] = 'Clube do Whisky - Marcas'
					AND pr.formItemOptionText <> 'JW RED LABEL'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				)
			 AS VARCHAR(50))
			, 'NA'
		)
	) AS [Clube do Whisky - JW RED LABEL]
	,
	(
		COALESCE(
			CAST(
				(
					SELECT MAX(pr.measureAnswer)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					AND pr.planResultDetailIsDeleted = 0
					--AND pr.formItemStatement = 'SELECIONE AS MARCAS QUE EST�O PRESENTES NO CLUBE DO WISKY'
					AND pr.[Form Promo��o Name] = 'Clube do Whisky - Marcas'
					AND pr.formItemOptionText = 'JW BLACK LABEL'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				)
			 AS VARCHAR(50))
			, CAST(
				0 * (
					SELECT MAX(pr.measureAnswer)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					AND pr.planResultDetailIsDeleted = 0
					--AND pr.formItemStatement = 'SELECIONE AS MARCAS QUE EST�O PRESENTES NO CLUBE DO WISKY'
					AND pr.[Form Promo��o Name] = 'Clube do Whisky - Marcas'
					AND pr.formItemOptionText <> 'JW BLACK LABEL'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				)
			 AS VARCHAR(50))
			, 'NA'
		)
	) AS [Clube do Whisky - JW BLACK LABEL]
	,
	(
		COALESCE(
			CAST(
				(
					SELECT MAX(pr.measureAnswer)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					AND pr.planResultDetailIsDeleted = 0
					--AND pr.formItemStatement = 'SELECIONE AS MARCAS QUE EST�O PRESENTES NO CLUBE DO WISKY'
					AND pr.[Form Promo��o Name] = 'Clube do Whisky - Marcas'
					AND pr.formItemOptionText = 'OLD PARR'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				)
			 AS VARCHAR(50))
			, CAST(
				0 * (
					SELECT MAX(pr.measureAnswer)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					AND pr.planResultDetailIsDeleted = 0
					--AND pr.formItemStatement = 'SELECIONE AS MARCAS QUE EST�O PRESENTES NO CLUBE DO WISKY'
					AND pr.[Form Promo��o Name] = 'Clube do Whisky - Marcas'
					AND pr.formItemOptionText <> 'OLD PARR'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				)
			 AS VARCHAR(50))
			, 'NA'
		)
	) AS [Clube do Whisky - OLD PARR]
	,
	(
		COALESCE(
			CAST(
				(
					SELECT MAX(pr.measureAnswer)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					AND pr.planResultDetailIsDeleted = 0
					--AND pr.formItemStatement = 'SELECIONE AS MARCAS QUE EST�O PRESENTES NO CLUBE DO WISKY'
					AND pr.[Form Promo��o Name] = 'Clube do Whisky - Marcas'
					AND pr.formItemOptionText = 'WHITE HORSE'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				)
			 AS VARCHAR(50))
			, CAST(
				0 * (
					SELECT MAX(pr.measureAnswer)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					AND pr.planResultDetailIsDeleted = 0
					--AND pr.formItemStatement = 'SELECIONE AS MARCAS QUE EST�O PRESENTES NO CLUBE DO WISKY'
					AND pr.[Form Promo��o Name] = 'Clube do Whisky - Marcas'
					AND pr.formItemOptionText <> 'WHITE HORSE'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				)
			 AS VARCHAR(50))
			, 'NA'
		)
	) AS [Clube do Whisky - WHITE HORSE]
	,
	(
		COALESCE(
			CAST(
				(
					SELECT MAX(pr.measureAnswer)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					AND pr.planResultDetailIsDeleted = 0
					--AND pr.formItemStatement = 'SELECIONE AS MARCAS QUE EST�O PRESENTES NO CLUBE DO WISKY'
					AND pr.[Form Promo��o Name] = 'Clube do Whisky - Marcas'
					AND pr.formItemOptionText = 'BLACK & WHITE'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				)
			 AS VARCHAR(50))
			, CAST(
				0 * (
					SELECT MAX(pr.measureAnswer)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					AND pr.planResultDetailIsDeleted = 0
					--AND pr.formItemStatement = 'SELECIONE AS MARCAS QUE EST�O PRESENTES NO CLUBE DO WISKY'
					AND pr.[Form Promo��o Name] = 'Clube do Whisky - Marcas'
					AND pr.formItemOptionText <> 'BLACK & WHITE'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				)
			 AS VARCHAR(50))
			, 'NA'
		)
	) AS [Clube do Whisky - BLACK & WHITE]
	,
	(
		COALESCE(
			CAST(
				(
					SELECT MAX(pr.measureAnswer)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					AND pr.planResultDetailIsDeleted = 0
					--AND pr.formItemStatement = 'SELECIONE AS MARCAS QUE EST�O PRESENTES NO CLUBE DO WISKY'
					AND pr.[Form Promo��o Name] = 'Clube do Whisky - Marcas'
					AND pr.formItemOptionText = 'OUTROS'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				)
			 AS VARCHAR(50))
			, CAST(
				0 * (
					SELECT MAX(pr.measureAnswer)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					AND pr.planResultDetailIsDeleted = 0
					--AND pr.formItemStatement = 'SELECIONE AS MARCAS QUE EST�O PRESENTES NO CLUBE DO WISKY'
					AND pr.[Form Promo��o Name] = 'Clube do Whisky - Marcas'
					AND pr.formItemOptionText <> 'OUTROS'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				)
			 AS VARCHAR(50))
			, 'NA'
		)
	) AS [Clube do Whisky - OUTROS]
	,
	(
		COALESCE(
			CAST(
				(
					SELECT MAX(pr.measureAnswer)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					AND pr.planResultDetailIsDeleted = 0
					AND pr.formItemStatement = 'A CASA EST� ADERIDA AO PROGRAMA DE RELACIONAMENTO TOP BAR?'
					--AND pr.[Form Promo��o Name] = 'Top Bar'
					AND pr.formItemOptionText = 'SIM'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				)
			 AS VARCHAR(50))
			, CAST(
				0 * (
					SELECT MAX(pr.measureAnswer)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					AND pr.planResultDetailIsDeleted = 0
					AND pr.formItemStatement = 'A CASA EST� ADERIDA AO PROGRAMA DE RELACIONAMENTO TOP BAR?'
					--AND pr.[Form Promo��o Name] = 'Top Bar'
					AND pr.formItemOptionText <> 'SIM'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				)
			 AS VARCHAR(50))
			, 'NA'
		)
	) AS [Top Bar]
	,
	(
		COALESCE(
			CAST(
				(
					SELECT MAX(pr.measureAnswer)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					AND pr.planResultDetailIsDeleted = 0
					AND pr.formItemStatement = 'A CASA POSSUI O TABLE TENT E A��O "TANQUERAY HOUR + KETEL ONE" IMPLEMENTADA ?'
					--AND pr.[Form Promo��o Name] = 'Table Tent "TANQUERAY HOUR + KETEL ONE"'
					AND pr.formItemOptionText = 'SIM'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				)
			 AS VARCHAR(50))
			, CAST(
				0 * (
					SELECT MAX(pr.measureAnswer)
					FROM [DW].vw_PRDFIO AS pr
					WHERE pr.locationId = l.locationId 
					AND pr.dateAudited BETWEEN @startDate AND @endDate
					AND pr.isCompleted = 1
					AND pr.planResultDetailIsDeleted = 0
					AND pr.formItemStatement = 'A CASA POSSUI O TABLE TENT E A��O "TANQUERAY HOUR + KETEL ONE" IMPLEMENTADA ?'
					--AND pr.[Form Promo��o Name] = 'Table Tent "TANQUERAY HOUR + KETEL ONE"'
					AND pr.formItemOptionText <> 'SIM'
					AND pr.[Form Template Form Type Code] = @formTypeCode
					AND pr.auditedBy NOT LIKE '%test%'
				)
			 AS VARCHAR(50))
			, 'NA'
		)
	) AS [Table Tent "TANQUERAY HOUR + KETEL ONE"]
FROM t AS l
